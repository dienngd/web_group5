<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8BOM">
<title>Giỏ hàng</title>
<link href="css/style.css" rel="stylesheet">
<link rel="icon" href="./images/icon2.png" type="images/x-icon"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>   
	 <?php
	       session_start();
	 ?> 
	 <?php include "banner.php";?>

<br>
<br>
    <?php include "menu.php";?>
        
<br>
    <?php include "anhdong.php";?>
<?php
	if(isset($_GET["masanpham"]) && isset($_GET["so_luong"])){
		$masanpham = $_GET["masanpham"];
		// Bước 1: Kết nối đến CSDL
		include("./config/dbconfig.php");
     	$ket_noi = mysqli_connect($dbhost,$dbuser,$dbpassword,$dbname);
        $sql = "
                SELECT *
                FROM `sanpham`
				WHERE masanpham='$masanpham' ";
		$du_lieu = mysqli_query($ket_noi,$sql);
		$data = mysqli_fetch_array($du_lieu);
		$_SESSION['gio_hang']['mat_hang'][$_GET["masanpham"]] = array(
			'masanpham' => $data[0],
            'tensanpham' => $data[1],
            'dongiaban' => $data[2],
            'so_luong'=> (int)$_GET["so_luong"],
			'anh'=>$data[8]
		);
	}
;?>           
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				
			
			</div>
			<div class="table-responsive cart_info" style="width:90%; margin-left:5%" >
				<table class="table table-condensed" cellpadding="5px" cellspacing="1px" width="100%">
					<thead>
						<tr class="cart_menu">
							
							<td class="image" style="width:20%">Sản phẩm  </td>		
							<td class="description" style="width:32%">Tên sản phẩm</td>
							<td class="price" style="width:18%">Giá</td>
							<td class="quantity" style="width:12%">Số lượng</td>
							<td class="total" style="width:20%">Thành tiền</td>
							<td style=""></td>
							<td style=""></td>
						
						</tr>		
					</thead>
		
                     <?php       
                    if(isset($_SESSION['gio_hang']) && $_SESSION['gio_hang']['tong_so'] > 0){
                   
                    $tongTien=0;
                    foreach($_SESSION['gio_hang']['mat_hang'] as $matHang =>$value){
                        $thanhTien = $value['so_luong'] * $value['dongiaban'];
                        $tongTien += $thanhTien;
                ?>
					<tbody>
						<tr>  
							
							<td class="cart_product" style="text-align: center;margin: 5px -0px 0px 0px;">
                                
								<img src="<?=$value['anh']?>" alt="" style="width: 100%; height:100%; " />
							
							</td>

							<td class="cart_description" style="text-align: center;">
								<h4><a style="text-decoration:none;color:#6E4D8B;font-size:22px" href="c_SanPhamChiTiet.php?masanpham=<?=$value['masanpham']?>"><?=$value['tensanpham']?></a></h4>
					
							</td>

							<td class="cart_price" style="text-align: center; margin-top:10px;">
								<p style="font-size:20px; color:#6E4D8B;"><?=$value['dongiaban']?> VNĐ</p>
							</td>

							<td class="cart_quantity" style="text-align: center">
								<div class="cart_quantity_button">
								<input type="number" onchange='capnhatsoluong' name="qty_<?php echo $matHang?>" id="qty_<?php echo $matHang?>" value="<?=$value['so_luong']?>" min='1' max='100'>
								</div>						
							</td>
			
							<td class="cart_total">
								<p class="cart_total_price" style="text-align: center; color:#6E4D8B"><?=$thanhTien?> VNĐ</p>
							</td>

							<td  class="text-center">
                               <a href="xoa_gio_hang.php?masanpham=<?php echo $matHang?>" title="Xóa"><img src="./images/delete.png" style="width:35px; height:35px"></a>

						    </td>
					
					        <td  class="text-center">
                                <a href="javascript:void(0)" onclick="updateitem(<?php echo $matHang?>)"  title="Cập nhật" ><img src="./images/update.png" style="width:35px; height:35px"></a>
                            </td>

						</tr>
				
                         <?php
                   
					}
					
                    $_SESSION['gio_hang']['tong_tien']=$tongTien;
                ?>
				
                <tr>
                    <td style="font-size:22px" colspan="7" class="text-left">
                        Tổng giá trị đơn hàng: <strong style="font-size: 22px;color:red" class="text-primary"><?=$tongTien?> VNĐ</strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="text-center">
                        <a style="background-color:#6E4D8B; font-size:22px; color:white; text-decoration: none; float:right; width:100px" href="hoa_don.php" class="btn btn-info">Thanh toán</a>
                    </td>
                </tr>
                <?php   
                }
                else{
                ?>
                <tr>
                    <td colspan="5">Bạn chưa mua sản phẩm nào</td>
                </tr>
                <?php
                }
                ?>
					</body>
				</table>
				<script>
					function updateitem(matHang){
						soluong= $('#qty_'+matHang).val();
						$.get("gio_hang.php?masanpham="+matHang+"&so_luong="+soluong, function(data){
								location.reload();
								
						});
					}
				</script>
			</div>
		</div>
    </section> <!--/#cart_items-->

	<?php include "footer.php" ;?>