-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 30, 2018 lúc 11:43 AM
-- Phiên bản máy phục vụ: 10.1.32-MariaDB
-- Phiên bản PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `dbsb`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_chi_tiet_hoa_don`
--

CREATE TABLE `tbl_chi_tiet_hoa_don` (
  `id` int(11) NOT NULL,
  `id_hoa_don` int(11) DEFAULT NULL,
  `id_sp` int(11) DEFAULT NULL,
  `so_luong` int(11) NOT NULL,
  `gia` int(11) NOT NULL,
  `thanh_tien` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_chi_tiet_hoa_don`
--

INSERT INTO `tbl_chi_tiet_hoa_don` (`id`, `id_hoa_don`, `id_sp`, `so_luong`, `gia`, `thanh_tien`) VALUES
(1, 33, 21, 3, 18000, 54000),
(2, 33, 23, 1, 15000, 15000),
(3, 34, 33, 1, 230000, 230000),
(4, 34, 26, 3, 20000, 60000),
(5, 35, 23, 2, 15000, 30000),
(6, 35, 30, 1, 15000, 15000),
(7, 35, 21, 3, 18000, 54000),
(13, 37, 21, 2, 18000, 36000),
(14, 37, 27, 2, 45000, 90000),
(15, 38, 8, 1, 280000, 280000),
(16, 38, 3, 1, 270000, 270000),
(17, 38, 24, 2, 15000, 30000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_hoa_don`
--

CREATE TABLE `tbl_hoa_don` (
  `id_hoa_don` int(11) NOT NULL,
  `ngay_thang` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ten_kh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tong_tien` int(255) NOT NULL,
  `ngay_giao` date NOT NULL,
  `dia_chi_giao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dien_thoai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trang_thai` int(11) NOT NULL,
  `ghi_chu` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_hoa_don`
--

INSERT INTO `tbl_hoa_don` (`id_hoa_don`, `ngay_thang`, `ten_kh`, `tong_tien`, `ngay_giao`, `dia_chi_giao`, `dien_thoai`, `trang_thai`, `ghi_chu`) VALUES
(33, '2018-06-23 14:10:00', 'ÄoÃ n HÃ  My', 69000, '2018-06-29', '12/32/176 TrÆ°Æ¡ng Äá»‹nh, HÃ  Ná»™i', '0989201997', 1, 'Giao hÃ ng buá»•i chiá»u'),
(34, '2018-06-23 09:20:12', 'VÅ© Thá»§y TiÃªn', 290000, '2018-07-06', 'Sá»‘ 12 ChÃ¹a Bá»™c', '0169868009', 0, ''),
(35, '2018-06-23 10:50:12', 'Pháº¡m Quá»³nh Anh', 99000, '2018-07-02', 'Phá»‘ Huáº¿', '0934617946', 0, ''),
(37, '2018-06-25 15:30:48', 'Tráº§n Má»¹ Linh', 126000, '2018-06-30', 'sá»‘ 12 báº¡ch mai', '01698368384', 1, 'gá»i trÆ°á»›c khi Ä‘áº¿n'),
(38, '2018-06-30 09:22:22', 'Nguyá»…n Thanh BÃ¬nh', 580000, '2018-07-04', 'Há»c viá»‡n ngÃ¢n hÃ ng', '01696306066', 0, 'LiÃªn há»‡ trÆ°á»›c khi Ä‘áº¿n');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_lien_he`
--

CREATE TABLE `tbl_lien_he` (
  `id_lien_he` int(11) NOT NULL,
  `ten_lien_he` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dien_thoai` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noi_dung` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_lien_he`
--

INSERT INTO `tbl_lien_he` (`id_lien_he`, `ten_lien_he`, `dien_thoai`, `email`, `noi_dung`) VALUES
(1, 'ÄoÃ n HÃ  My', '0989201997', 'tieumieu110@gmail.com', 'MÃ¬nh ráº¥t thÃ­ch bÃ¡nh cá»§a Savoury Bakery. Sáº½ á»§ng há»™ cá»­a hÃ ng nhiá»u ^^.'),
(2, 'Pháº¡m Quá»³nh Anh', '0934617946', 'pquanh04@gmail.com', 'Ráº¥t thÃ­ch sáº£n pháº©m cá»§a shop');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_san_pham`
--

CREATE TABLE `tbl_san_pham` (
  `id_sp` int(11) NOT NULL,
  `ten_sp` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinh_anh` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_san_pham`
--

INSERT INTO `tbl_san_pham` (`id_sp`, `ten_sp`, `gia`, `hinh_anh`, `loai`) VALUES
(1, 'BÃ¡nh Mousse Tiramisu', '300000', 'images/mousse 3.jpg', 3),
(2, 'BÃ¡nh Mousse Chocolate', '270000', 'images/mousse 6.jpg', 3),
(3, 'BÃ¡nh Mousse TrÃ  Xanh', '270000', 'images/mousse 9.jpg', 3),
(4, 'BÃ¡nh Mousse DÃ¢u TÃ¢y', '270000', 'images/10.jpg', 3),
(5, 'BÃ¡nh quy bÆ¡ Cranberry', '30000', 'images/quy 1.jpg', 4),
(6, 'BÃ¡nh lÆ°á»¡i mÃ¨o (3 vá»‹)', '25000', 'images/banhquy.jpg', 4),
(7, 'BÃ¡nh quy hoa anh Ä‘Ã o', '25000', 'images/quy 3.jpg', 4),
(8, 'BÃ¡nh Mouse Chanh Leo', '280000', 'images/mousse 2.png', 3),
(9, 'BÃ¡nh Mousse Hawaii', '280000', 'images/mousse 4.png', 3),
(10, 'BÃ¡nh quy chocolate chip', '30000', 'images/quy 6.jpg', 4),
(11, 'BÃ¡nh quy bÆ¡ vá»‹ trÃ  xanh', '28000', 'images/quy 5.jpg', 4),
(12, 'BÃ¡nh quy háº¡nh nhÃ¢n', '28000', 'images/quy 7.jpg', 4),
(13, 'BÃ¡nh Black Velvet', '250000', 'images/gato3.png', 1),
(14, 'BÃ¡nh Gato Coffee', '240000', 'images/gato1.png', 1),
(15, 'BÃ¡nh Gato DÃ¢u', '240000', 'images/gato6.png', 1),
(17, 'BÃ¡nh Gato XoÃ i', '240000', 'images/gato5.png', 1),
(18, 'BÃ¡nh Gato TrÃ  Xanh', '250000', 'images/gato2.png', 1),
(19, 'PhÃ´ Mai Nháº­t Báº£n', '20000', 'images/cat1.png', 2),
(20, 'Tiramisu Truyá»n Thá»‘ng', '18000', 'images/j.jpg', 2),
(21, 'Tiramisu TrÃ  Xanh', '18000', 'images/cat3.png', 2),
(22, 'Mousse DÃ¢u TÃ¢y', '15000', 'images/ls.jpg', 2),
(23, 'BÃ¡nh Socola', '15000', 'images/001.jpg', 2),
(24, 'BÃ¡nh Cuá»™n Hoa Quáº£', '15000', 'images/cat2.png', 2),
(25, 'BÃ¡nh MÃ¬ Gá»‘i', '15000', 'images/banhmy1.jpg', 5),
(26, 'BÃ¡nh MÃ¬ BÆ¡ Má»m', '20000', 'images/banhmi3.jpg', 5),
(27, 'BÃ¡nh MÃ¬ Hoa CÃºc', '45000', 'images/banhmi2.jpg', 5),
(28, 'BÃ¡nh MÃ¬ BÃ­ Äá»', '20000', 'images/banhmi5.jpg', 5),
(29, 'BÃ¡nh MÃ¬ ChÃ  BÃ´ng', '18000', 'images/banhmi6.jpg', 5),
(30, 'BÃ¡nh MÃ¬ Hokkaido', '15000', 'images/banhmi4.jpg', 5),
(31, 'BÃ¡nh CÆ°á»›i 01', '200000', 'images/1.2.jpg', 6),
(32, 'BÃ¡nh CÆ°á»›i 02', '300000', 'images/2.3.jpg', 6),
(33, 'BÃ¡nh CÆ°á»›i 03', '230000', 'images/1.1.jpg', 6),
(34, 'BÃ¡nh CÆ°á»›i 04', '280000', 'images/banhcuoi1.jpg', 6),
(35, 'BÃ¡nh CÆ°á»›i 05', '320000', 'images/banhcuoi02.png', 6),
(36, 'BÃ¡nh CÆ°á»›i 06', '420000', 'images/3.4.jpg', 6),
(37, 'BÃ¡nh Gato Chocolate', '230000', 'images/banhgato.jpg', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_tin_tuc`
--

CREATE TABLE `tbl_tin_tuc` (
  `id_tin_tuc` int(11) NOT NULL,
  `ten_tt` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mo_ta_tt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `noi_dung_tt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinh_anh_tt` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_tin_tuc`
--

INSERT INTO `tbl_tin_tuc` (`id_tin_tuc`, `ten_tt`, `mo_ta_tt`, `noi_dung_tt`, `hinh_anh_tt`) VALUES
(5, 'KHAI TRÆ¯Æ NG Cá»¬A HÃ€NG - GIáº¢M GIÃ 10% Táº¤T Cáº¢ Sáº¢N PHáº¨M', '<p style=\"text-align: justify;\">Cá»­a h&agrave;ng Savoury Bakery Ch&ugrave;a Bá»™c sáº½ ch&iacute;nh thá»©c má»Ÿ cá»­a v&agrave;o ng&agrave;y ng&agrave;y 1/6/2018. Nh&acirc;n dá»‹p khai trÆ°Æ¡ng, Savoury Bakery c&oacute; chÆ°Æ¡ng tr&igrave;nh khuyáº¿n m&atilde;i háº¥p dáº«n: &ldquo;Æ¯u Ä‘&atilde;i th&aacute;ng v&agrave;ng khai trÆ°Æ¡ng &ndash; Giáº£m 10% cho táº¥t cáº£ c&aacute;c sáº£n pháº©m b&aacute;nh mua táº¡i cá»­a h&agrave;ng</p>', '<p style=\"text-align: justify;\">Tá»a láº¡c tr&ecirc;n má»™t trong nhá»¯ng cung Ä‘Æ°á»ng nhá»™n nhá»‹p á»Ÿ H&agrave; Ná»™i, táº¡i sá»‘ 12 Ch&ugrave;a Bá»™c, PhÆ°á»ng Quang Trung, Quáº­n Äá»‘ng Äa Savoury Bakery há»©a háº¹n mang Ä‘áº¿n nhá»¯ng hÆ°Æ¡ng vá»‹ tháº­t má»›i máº», Ä‘&aacute;p á»©ng Ä‘Æ°á»£c Ä‘áº§y Ä‘á»§ nhu cáº§u cho c&aacute;c t&iacute;n Ä‘á»“ s&agrave;nh b&aacute;nh, l&agrave; m&oacute;n qu&agrave; Ä‘áº·c biá»‡t d&agrave;nh cho táº·ng cho qu&yacute; kh&aacute;ch h&agrave;ng Ä‘&atilde; v&agrave; Ä‘ang tin tÆ°á»Ÿng, y&ecirc;u qu&yacute; Savoury Bakery trong suá»‘t thá»i gian vá»«a qua. <br /><br />Kh&ocirc;ng gian cá»­a h&agrave;ng Savoury Bakery Ä‘Æ°á»£c x&acirc;y dá»±ng rá»™ng r&atilde;i, tho&aacute;ng m&aacute;t vá»›i c&aacute;c quáº§y/ká»‡ b&aacute;nh Ä‘Æ°á»£c b&agrave;y tr&iacute; Ä‘áº§y áº¯p nhá»¯ng hÆ°Æ¡ng vá»‹ quen thuá»™c nhÆ°: B&aacute;nh Pate Chaud n&oacute;ng há»•i, b&aacute;nh Croissant thÆ¡m lá»«ng hay Lá»›p kem sá»¯a tÆ°Æ¡i nháºµn má»‹n, hÆ°Æ¡ng chocolate Ä‘áº·c trÆ°ng cá»§a b&aacute;nh kem... táº¥t cáº£ Ä‘á»u Ä‘Æ°á»£c trang tr&iacute; tháº­t sang trá»ng v&agrave; báº¯t máº¯t. <br /><br />Thá»i gian khuyáº¿n m&atilde;i: Tá»« 01/06/2018 Ä‘áº¿n 10/06/2018. <br /><br />Äá»‹a Ä‘iá»ƒm khuyáº¿n m&atilde;i: &Aacute;p dá»¥ng táº¡i cá»­a h&agrave;ng Savoury Bakery sá»‘ 12 Ch&ugrave;a Bá»™c.</p>', 'images/km1.jpg'),
(6, 'THÃNG 5 - CÃC Sáº¢N PHáº¨M BÃNH KEM CHá»ˆ Tá»ª 180K', '<p>N&oacute;ng qu&aacute; n&oacute;ng qu&aacute; n&oacute;ng qu&aacute; Ä‘i! Th&aacute;ng 5 tá»›i rá»“i, m&ugrave;a h&egrave; cÅ©ng tá»›i rá»“i, báº§u kh&ocirc;ng kh&iacute; sáº½ trá»Ÿ n&ecirc;n v&ocirc; c&ugrave;ng n&oacute;ng bá»©c kh&oacute; chá»‹u má»—i trÆ°a vá». Haizzzzzzzzz, nghÄ© tá»›i m&agrave; náº£n l&ograve;ng qu&aacute;, cháº¯c láº¡i tá»‘n qu&aacute; trá»i tiá»n mua kem Äƒn rá»“i ^^.</p>', '<p>Ráº¥t nhiá»u máº«u b&aacute;nh kem Ä‘ang chá» Ä‘&oacute;n c&aacute;c báº¡n. H&atilde;y nhanh ch&acirc;n tá»›i Savoyry Bakery Ä‘á»ƒ mua nhá»¯ng chiáº¿c b&aacute;nh d&agrave;nh táº·ng cho má»™t ná»­a cá»§a m&igrave;nh hoáº·c nhá»¯ng ngÆ°á»i th&acirc;n y&ecirc;u n&agrave;o ^^.<br /><br /><strong>Thá»i gian khuyáº¿n m&atilde;i:&nbsp;</strong>Tá»« 01/05/2018 Ä‘áº¿n 30/05/2018.<br /><br /><strong>Äá»‹a Ä‘iá»ƒm khuyáº¿n m&atilde;i:</strong> &Aacute;p dá»¥ng táº¡i cá»­a h&agrave;ng Savoury Bakery sá»‘ 12 Ch&ugrave;a Bá»™c.</p>', 'images/km2.jpg'),
(8, ' HÆ¯á»šNG DáºªN LÃ€M BÃNH SU KEM Táº I NHÃ€', '<p>Báº¡n l&agrave; t&iacute;n Ä‘á»“ cá»§a su kem ? Báº¡n m&ecirc; máº©n hÆ°Æ¡ng vá»‹ thÆ¡m b&eacute;o cá»§a nhá»¯ng chiáº¿c su kem b&eacute; xinh ? Tháº­t Ä‘Æ¡n giáº£n, giá» Ä‘&acirc;y báº¡n c&oacute; thá»ƒ tá»± tay l&agrave;m nhá»¯ng chiáº¿c su kem ngon tuyá»‡t d&agrave;nh táº·ng cho nhá»¯ng ngÆ°á»i th&acirc;n y&ecirc;u cá»§a m&igrave;nh rá»“i. H&atilde;y c&ugrave;ng m&igrave;nh v&agrave;o báº¿p Ä‘á»ƒ thá»±c hiá»‡n nh&eacute;.</p>', '<p><strong>NGUY&Ecirc;N LIá»†U:</strong> <br />- 2 l&ograve;ng Ä‘á» trá»©ng (18 &ndash; 20 gr/ l&ograve;ng Ä‘á») <br />- 90 gr sá»¯a Ä‘áº·c &Ocirc;ng Thá» <br />- 20 gram tinh bá»™t ng&ocirc; <br />- 200 gram sá»¯a tÆ°Æ¡i kh&ocirc;ng Ä‘Æ°á»ng <br />- 30 gram bÆ¡ nháº¡t (bÆ¡ Ä‘á»™ng váº­t kh&ocirc;ng muá»‘i) <br />- &frac14; th&igrave;a caf&eacute; vani chiáº¿t xuáº¥t (kh&ocirc;ng báº¯t buá»™c) <br />- 20 gram sá»¯a Ä‘áº·c c&oacute; Ä‘Æ°á»ng (Ä‘á»ƒ trá»™n v&agrave;o sau c&ugrave;ng, kh&aacute;c pháº§n 90 gr á»Ÿ tr&ecirc;n) <br /><br /><strong>C&Aacute;CH L&Agrave;M:</strong> <br />1. Cho l&ograve;ng Ä‘á» trá»©ng v&agrave; 90 gr sá»¯a v&agrave;o &acirc;u. Ä&aacute;nh Ä‘á»u Ä‘áº¿n khi h&ograve;a quyá»‡n, má»‹n mÆ°á»£t. R&acirc;y bá»™t v&agrave;o &acirc;u. Trá»™n Ä‘á»u. <br />2. Cho sá»¯a v&agrave;o, trá»™n Ä‘á»u. Lá»c há»—n há»£p qua r&acirc;y, cho v&agrave;o ná»“i.<br />3. Äun há»—n há»£p á»Ÿ lá»­a vá»«a. Quáº¥y Ä‘á»u trong khi Ä‘un. Khi há»—n há»£p sá»‡t láº¡i v&agrave; náº¿m thá»­ kh&ocirc;ng tháº¥y c&ograve;n vá»‹ bá»™t ná»¯a th&igrave; báº¯c khá»i báº¿p. Láº­p tá»©c cho bÆ¡ v&agrave; vanilla v&agrave;o, trá»™n Ä‘á»u tá»›i khi bÆ¡ tan cháº£y háº¿t v&agrave; h&ograve;a quyá»‡n. Cuá»‘i c&ugrave;ng trá»™n pháº§n 20 gram sá»¯a Ä‘áº·c v&agrave;o. Viá»‡c trá»™n sá»¯a v&agrave;o sau sáº½ gi&uacute;p cho nh&acirc;n c&oacute; vá»‹ ngá»t, thÆ¡m v&agrave; ngáº­y b&eacute;o hÆ¡n.<br /><br /><em><strong>* LÆ¯U &Yacute; QUAN TRá»ŒNG:</strong></em> Náº¿u kh&ocirc;ng quen Äƒn ngá»t nhiá»u, c&aacute;c báº¡n c&oacute; thá»ƒ cho 3/4 lÆ°á»£ng sá»¯a Ä‘áº·c v&agrave;o Ä‘&aacute;nh c&ugrave;ng trá»©ng trÆ°á»›c, tá»›i khi xong bÆ°á»›c 3 th&igrave; n&ecirc;m náº¿m láº¡i v&agrave; tiáº¿p tá»¥c cho sá»¯a Ä‘á»ƒ nh&acirc;n vá»«a ngá»t nh&eacute;.<br />Äá»ƒ nh&acirc;n nguá»™i rá»“i d&ugrave;ng.</p>', 'images/tintuc4.jpg'),
(10, 'KHUYáº¾N MÃƒI NGÃ€Y Cá»¦A CHA - GIáº¢M 50% BÃNH KEM Láº NH', '<p>Tuáº§n lá»… tri &acirc;n Ng&agrave;y cá»§a cha: giáº£m gi&aacute; 30% c&aacute;c loáº¡i b&aacute;nh kem láº¡nh nh&acirc;n ng&agrave;y cá»§a cha tá»« 13/6 Ä‘áº¿n 17/6, táº·ng thiá»‡p ch&uacute;c má»«ng v&agrave; voucher ng&agrave;y 18 v&agrave; 19/6. Nhanh ch&acirc;n &ldquo;táº­u&rdquo; b&aacute;nh kem láº¡nh Ä‘á»ƒ táº·ng cho nhá»¯ng ngÆ°á»i Ä‘&agrave;n &ocirc;ng c&oacute; &yacute; nghÄ©a trong cuá»™c Ä‘á»i m&igrave;nh n&agrave;o c&aacute;c Fan Æ¡i !</p>', '<p>NhÆ° Ä‘&atilde; há»©a, h&ocirc;m nay Savoury Bakery c&ocirc;ng bá»‘ ra máº¯t chÆ°Æ¡ng tr&igrave;nh Tuáº§n lá»… tri &acirc;n Cha d&agrave;nh cho c&aacute;c fan b&aacute;nh kem láº¡nh v&agrave; muá»‘n d&agrave;nh táº·ng ngÆ°á»i cha th&acirc;n y&ecirc;u má»™t ng&agrave;y tháº­t Ä‘áº·t biá»‡t. <br /><br />Tuáº§n lá»… tri &acirc;n Ng&agrave;y cá»§a cha: giáº£m gi&aacute; 50% c&aacute;c loáº¡i b&aacute;nh kem láº¡nh nh&acirc;n ng&agrave;y cá»§a cha tá»« 13/6 Ä‘áº¿n 17/6, táº·ng thiá»‡p ch&uacute;c má»«ng v&agrave; voucher ng&agrave;y 18 v&agrave; 19/6.<br /><br /><strong>Thá»i gian khuyáº¿n m&atilde;i:</strong> Tá»« 13/06/2018 Ä‘áº¿n 17/06/2018.<br /><br /><strong>Äá»‹a Ä‘iá»ƒm khuyáº¿n m&atilde;i:</strong> &Aacute;p dá»¥ng táº¡i cá»­a h&agrave;ng Savoury Bakery sá»‘ 12 Ch&ugrave;a Bá»™c.</p>', 'images/km3.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `user_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `user_name`, `password`) VALUES
(1, 'admin', 'admin123');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_chi_tiet_hoa_don`
--
ALTER TABLE `tbl_chi_tiet_hoa_don`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sp` (`id_sp`),
  ADD KEY `id_hoa_don` (`id_hoa_don`);

--
-- Chỉ mục cho bảng `tbl_hoa_don`
--
ALTER TABLE `tbl_hoa_don`
  ADD PRIMARY KEY (`id_hoa_don`);

--
-- Chỉ mục cho bảng `tbl_lien_he`
--
ALTER TABLE `tbl_lien_he`
  ADD PRIMARY KEY (`id_lien_he`);

--
-- Chỉ mục cho bảng `tbl_san_pham`
--
ALTER TABLE `tbl_san_pham`
  ADD PRIMARY KEY (`id_sp`),
  ADD KEY `id_loai` (`loai`);

--
-- Chỉ mục cho bảng `tbl_tin_tuc`
--
ALTER TABLE `tbl_tin_tuc`
  ADD PRIMARY KEY (`id_tin_tuc`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_chi_tiet_hoa_don`
--
ALTER TABLE `tbl_chi_tiet_hoa_don`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `tbl_hoa_don`
--
ALTER TABLE `tbl_hoa_don`
  MODIFY `id_hoa_don` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT cho bảng `tbl_lien_he`
--
ALTER TABLE `tbl_lien_he`
  MODIFY `id_lien_he` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_san_pham`
--
ALTER TABLE `tbl_san_pham`
  MODIFY `id_sp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT cho bảng `tbl_tin_tuc`
--
ALTER TABLE `tbl_tin_tuc`
  MODIFY `id_tin_tuc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `tbl_chi_tiet_hoa_don`
--
ALTER TABLE `tbl_chi_tiet_hoa_don`
  ADD CONSTRAINT `tbl_chi_tiet_hoa_don_ibfk_1` FOREIGN KEY (`id_hoa_don`) REFERENCES `tbl_hoa_don` (`id_hoa_don`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_chi_tiet_hoa_don_ibfk_2` FOREIGN KEY (`id_sp`) REFERENCES `tbl_san_pham` (`id_sp`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
