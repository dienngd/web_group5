<!DOCTYPE html>
<html>
<head>
    <title>Chính sách vận chuyển</title>
    <link rel="icon" href="./images/icon2.png" type="images/x-icon"/>
    <link rel="stylesheet" type="text/css" href=".\css\style.css">
</head>

<body>
<?php include "banner.php";?>

<br>
<br>

    <?php include "menu.php";?>
        
 <br>       
    <?php include "anhdong.php";?>

<h2 class="page-title" style="margin-left: 40px;color:rgb(40, 16, 61);text-align:left;">CHÍNH SÁCH</h2>
<hr>

<div class="rte" style="margin-left: 100px; margin-right: 100px; text-align: justify; font-size:18px">
<p> Cám ơn quý khách đã quan tâm và truy cập vào website. Chúng tôi tôn trọng và cam kết sẽ bảo mật những thông tin mang tính riêng tư của Quý khách.</p>

<p>Chính sách bảo mật sẽ giải thích cách chúng tôi tiếp nhận, sử dụng và (trong trường hợp nào đó) tiết lộ thông tin cá nhân của Quý khách.</p>

<p>Bảo vệ dữ liệu cá nhân và gây dựng được niềm tin cho quý khách là vấn đề rất quan trọng với chúng tôi. </p>
<p>Vì vậy, chúng tôi sẽ dùng tên và các thông tin khác liên quan đến quý khách tuân thủ theo nội dung của Chính sách bảo mật. </p>

<p>Chúng tôi chỉ thu thập những thông tin cần thiết liên quan đến giao dịch mua bán.</p>

<p>Chúng tôi sẽ giữ thông tin của khách hàng trong thời gian luật pháp quy định hoặc cho mục đích nào đó. </p>

<p> Quý khách có thể truy cập vào website và trình duyệt mà không cần phải cung cấp chi tiết cá nhân.</p>

<p>Lúc đó, Quý khách đang ẩn danh và chúng tôi không thể biết bạn là ai nếu Quý khách không đăng nhập vào tài khoản của mình.</p>

<p><strong>1. Thu thập thông tin cá nhân</strong> </p>

<p>Chương trình mua hàng và cho những thông báo sau này liên quan đến đơn hàng, và để cung cấp dịch vụ, bao gồm một số thông tin cá nhân.</p>
                     
<p>Chúng tôi sẽ dùng thông tin quý khách đã cung cấp để xử lý đơn đặt hàng, cung cấp các dịch vụ và thông tin yêu cầu thông qua website và theo yêu cầu của bạn.</p>

<p>Quý khách cam kết bảo mật dữ liệu cá nhân và không được phép tiết lộ cho bên thứ ba. Chúng tôi không chịu bất kỳ trách nhiệm nào nếu đây không phải lỗi của chúng tôi.</p>

<p>Chúng tôi có thể dùng thông tin cá nhân của bạn để nghiên cứu thị trường. Mọi thông tin chi tiết sẽ được ẩn và chỉ được dùng để thống kê.</p>

<p> Quý khách có thể từ chối không tham gia bất cứ lúc nào.</p>

<p ><strong>2. Bảo mật</strong></p>

<p >Chúng tôi có biện pháp thích hợp về kỹ thuật và an ninh để ngăn chặn truy cập trái phép hoặc trái pháp luật hoặc mất mát hoặc tiêu hủy hoặc thiệt hại cho thông tin của bạn.</p>

<p>Chúng tôi khuyên quý khách không nên đưa thông tin chi tiết về việc thanh toán với bất kỳ ai bằng e-mail </p>

<p> chúng tôi không chịu trách nhiệm về những mất mát quý khách có thể gánh chịu trong việc trao đổi thông tin của quý khách qua internet hoặc email. </p>

<p> Khi có tài khoản bạn sẽ dễ dàng theo dõi được đơn hàng của mình</p>

<p>Mọi thông tin giao dịch sẽ được bảo mật nhưng trong trường hợp cơ quan pháp luật yêu cầu, chúng tôi sẽ buộc phải cung cấp những thông tin này cho các cơ quan pháp luật. </p>

<p>Các điều kiện, điều khoản và nội dung của trang web này được điều chỉnh bởi luật pháp Việt Nam và tòa án Việt Nam có thẩm quyền xem xét.</p>

<p ><strong>3. Quyền lợi khách hàng</strong></p>

<p>Quý khách có quyền yêu cầu truy cập vào dữ liệu cá nhân của mình, có quyền yêu cầu chúng tôi sửa lại những sai sót trong dữ liệu của bạn mà không mất phí. </p>

<p>Bất cứ lúc nào bạn cũng có quyền yêu cầu chúng tôi ngưng sử dụng dữ liệu cá nhân của bạn cho mục đích tiếp thị.</p>

<p>Trân trọng cảm ơn.</p>
</div>

<br>
<br>
<br>
    <?php include "footer.php" ;?>

</body>
</html>