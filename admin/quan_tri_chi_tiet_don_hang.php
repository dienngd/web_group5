
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/admin1.png" type="image/ico" />

    <title>Trang quản trị Đơn hàng</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="../js/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php
        include("top.php");
        ;?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
              <h1>THÔNG TIN ĐƠN HÀNG</h1>
              <br>
              <?php
              // Bước 1: Kết nối đến CSDL
              include("../config/dbconfig.php");
              $ket_noi = mysqli_connect($dbhost, $dbuser, $dbpassword, $dbname);
              
              // Lấy dữ liệu từ trên đường dẫn xuống
		         $id = $_GET["soHDB"];

              // Bước 2: Viết câu lệnh truy vấn thực thiện chèn dữ liệu vào bảng tbl_lien_he
              $sql = "SELECT * FROM `hoadonban` WHERE soHDB=".$id;
              
              //Bước 3: Thực thi câu lệnh SQL]
              $du_lieu = mysqli_query($ket_noi, $sql);

              //Bước 4: Trình bày dữ liệu lên trang Web
              while ($row = mysqli_fetch_array($du_lieu)) {
                ;?>

              <form method="post" action="thuc_hien_cap_nhat_don_hang.php" enctype="multipart/form-data">
              <div class="form-group">
              <input type="hidden" name="id" value="<?php echo $id;?>" >
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Tên khách hàng<span class="required"></span></label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" name="tenkh" readonly = "readonly" value="<?php echo $row["makhach"];?>">
                  </div>   
              </div>
              <br><br><br>
              <div class="form-group">
              <?php
               $ngaythang = $row["ngayban"];
               $ngaythangmoi = date("d-m-Y H:i:s", strtotime($ngaythang)); 
              ?> 
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Ngày đặt hàng<span class="required"></span></label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" name="ngaydat" readonly = "readonly" value="<?php echo $ngaythangmoi;?>">
                  </div>   
              </div>
              <br><br><br>
              <div class="form-group">
            
              <br><br><br>
             
           
              
                <div>
                <hr>
                <table class="table">
                    <thead>
                      <tr>
                        <th>STT</th>
                        <th>Sản phẩm</th>
                        <th>Giá</th>
                        <th>Số lượng</th>
                        <th>Thành tiền</th>
                      </tr>
                    </thead>
                <tbody>
                  <?php  $i=0;
                  $sql1 = "SELECT * FROM `chitiethdb` join `sanpham` on chitiethdb.masanpham=sanpham.masanpham   WHERE soHDB = ".$id;
                  $du_lieu1 = mysqli_query($ket_noi, $sql1);
              while ($row1 = mysqli_fetch_array($du_lieu1)) {
                
              $i++;
              ;?>
              <tr>
              <?php
             $sql2 = "SELECT * FROM `chitiethdb` where soHDB = ".$id;
             $du_lieu2 = mysqli_query($ket_noi, $sql2);
         $row2 = mysqli_fetch_array($du_lieu2);
            ?>

                        <th scope="row"><?php echo $i;?></th>
                      <!--  <td><img src="<?php echo '<img src='.$row1['anh'].' />';?>" alt="" style="width: 100%; height:100p%; " /></td> -->
                        <td><?php echo $row1["tensanpham"];?></td>
                        <td><?php echo $row1["gia"];?></td>
                        <td><?php echo $row2["soluong"];?></td>
                        <td><?php echo $row1["thanhtien"];?></td>
             </tr>

             <tr>
            
             </tr>
              <?php };?>
              <th style="font-size:18px" colspan="6" class="text-left">
                        Tổng tiền: <strong style="font-size: 20px; float:right" class="text-primary"><?php echo $row["tongtien"]?> VNĐ</strong>
                    </th>
              
                </div>
                </div></from>
                       
                <br><br><br>
              </div>
              </form>
            </div>
          </div>
          <?php };?>
        <!-- /page content -->

        <?php 
        ;?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
	
  </body>
</html>
