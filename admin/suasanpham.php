
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Trang quản trị</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php
        include("top.php");
        ;?>

<?php
	//Kết nối csdl
	include("connect.php");
	$dbc=mysqli_connect("localhost", "root", "", "nhom5");
	if(isset($_GET['masanpham']) && $masanpham = $_GET['masanpham']){
		$sql="SELECT * FROM sanpham WHERE masanpham='$masanpham'";

	$sanpham = mysqli_query($dbc,$sql);
		if(mysqli_num_rows($sanpham)>0){
			$arrsv = mysqli_fetch_array($sanpham);
		}
		if(isset($_POST['submit'])){
		$tensanpham = $_POST['tensanpham'];
        $loai = $_POST['loai'];
        $co= $_POST['co'];
        $mau = $_POST['mau'];
        $chatlieu = $_POST['chatlieu'];
        $ngaynhap = $_POST['ngaynhap'];
        $soluong = $_POST['soluong'];
        $anh = $_POST['anh'];
        $dongiaban = $_POST['dongiaban'];
        $name_anh = 'images/'.$anh;
			

			$update = mysqli_query($conn, "UPDATE sanpham SET tensanpham='$tensanpham',maloai='$loai', maco='$co', mamau='$mau', chatlieu='$chatlieu', ngaynhap='$ngaynhap', soluong='$soluong', anh='$name_anh', dongiaban= '$dongiaban'  WHERE masanpham='$masanpham'");
			if($update){
				echo "Cập nhật thành công";
			}else {
				echo "Cập nhật thất bại";
			}
		}
	
?>
<form name="sanpham" action="" method="POST" style="background-color: white;">
<table width="400px" border="0" align="center" style="margin: 10px auto;">
    <tr>
        <td align="center" style="font-weight: bold;">Tên sản phẩm</td>
        <td><input name="tensanpham" id="tensanpham" type="text" value="<?php echo $arrsv['tensanpham']; ?>" /> </td>
    </tr>

    <tr>
        <td align="center" style="font-weight: bold;">Mã loại</td>
        <td>
            <select name="loai">
                <?php
					
					 $loai = mysqli_query($conn, "SELECT maloai,tenloai FROM theloai");
					 if(mysqli_num_rows($loai)>0){
						 while($arrlop=mysqli_fetch_array($loai)){
							 if($arrlop['maloai'] == $arrsv['maloai']) $selected='selected="selected"'; else $selected='';
							 echo '<option '.$selected.' value="'.$arrlop['maloai'].'">'.$arrlop['tenloai'].'</option>';
						 }
					 }
				 ?>
				?>
            </select>
        </td>
	</tr>
	 <tr>
            <td align="center" style="font-weight: bold;">Mã cỡ</td>
            <td>
                
            <select name="co">
                <?php
					$co = mysqli_query($conn,"SELECT maco,tenco FROM co");
					if(mysqli_num_rows($co)>0){
						while($arrlop=mysqli_fetch_array($co)){
							if($arrlop['maco'] == $arrsv['maco']) $selected='selected="selected"'; else $selected='';
							echo '<option '.$selected.' value="'.$arrlop['maco'].'">'.$arrlop['tenco'].'</option>';
						}
					}
				?>
            </select>
            </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Mã màu</td>
            <td>
                
            <select name="mau">
                <?php
					$mau = mysqli_query($conn, "SELECT mamau,tenmau FROM mau");
					if(mysqli_num_rows($mau)>0){
						while($arrlop=mysqli_fetch_array($mau)){
							if($arrlop['mamau'] == $arrsv['mamau']) $selected='selected="selected"'; else $selected='';
							echo '<option '.$selected.' value="'.$arrlop['mamau'].'">'.$arrlop['tenmau'].'</option>';
						}
					}
				?>
            </select>
            </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Chất liệu</td>
            <td><input name="chatlieu" type="text"  value="<?php echo $arrsv['chatlieu']; ?>"/> </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Ngày nhập</td>
            <td><input name="ngaynhap" type="date"  value="<?php echo $arrsv['ngaynhap']; ?>"/> </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Số lượng</td>
            <td><input name="soluong" type="text" value="<?php echo $arrsv['soluong']; ?>" /> </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Ảnh</td>
            <td><input name="anh" type="file" value="<?php echo $arrsv['anh']; ?>"/> </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Đơn giá bán</td>
            <td><input name="dongiaban" type="text"  value="<?php echo $arrsv['dongiaban']; ?>"/> </td>
        </tr>

    
        <td colspan="2" align="center"><input name="submit" type="submit" value="Sửa" /> </td>
    </tr>
</table>
<?php
	}else {
		echo "Trang yêu cầu không tồn tại";
	}
?>
</form>



        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
	
  </body>
</html>
