﻿<html>
<head>
	<meta charset="utf-8" />
	<title>Thông tin sản phẩm</title>
	<link rel="stylesheet" href="css/style_main.css" />
</head>
<body>
	<div id="wrapper">
		<div id="menu">
			<ul>
				<li><a href="index.php?page=sanpham">Sản phẩm</a></li>
				<li><a href="index.php?page=themsanpham" >Thêm sản phẩm</a></li>
		
			</ul>
		</div>
		<?php 
			if(isset($_GET['page'])){
				$page = $_GET['page'];
				if($page=='sanpham'){
					include("sanpham.php");
				}else if($page=='themsanpham'){
                    include("themsanpham.php");
                }else if($page=='suasanpham'){
                    include("suasanpham.php");
                }else if($page=='xoasanpham'){
                    include("xoasanpham.php");
                }else{
					include("sanpham.php");
				}
			}
		?>
	</div>
</body>
</html>