
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Trang quản trị</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php
        include("top.php");
        ;?>

<?php
    include("connect.php");

    if(isset($_POST['submit'])){
        $masanpham = $_POST['masanpham'];
        $tensanpham = $_POST['tensanpham'];
        $loai = $_POST['loai'];
        $co= $_POST['co'];
        $mau = $_POST['mau'];
        $chatlieu = $_POST['chatlieu'];
        $ngaynhap = $_POST['ngaynhap'];
        $soluong = $_POST['soluong'];
        $anh = $_POST['anh'];
        $dongiaban = $_POST['dongiaban'];
        $name_anh = 'images/'.$anh;
        // tai day em kiem tra xem ma sp co trung hay khong 
        // if( $msp == $masanpham || $masanpham == null){
        //     // thong bao trung hoac rong, roi thoat ra
        // }
        // else{
        //     them san pham vao
        // }
        // em hieu chua 
        
        $check_code = mysqli_query($conn, "SELECT * FROM sanpham WHERE masanpham= '$masanpham'") ;
        if (mysqli_num_rows($check_code) == 0 && $masanpham != null) { 
            $insert = mysqli_query($conn, "INSERT INTO sanpham (masanpham, tensanpham, maloai, maco, mamau, chatlieu, ngaynhap, soluong, anh, dongiaban) values ('$masanpham','$tensanpham', '$loai', '$co', '$mau', '$chatlieu', 'ngaynhap', '$soluong', '$name_anh', '$dongiaban')") ;
        if($insert){
            echo "Thêm thành công";
        }else {
            echo "Thêm thất bại";
        }   
         } else { 
            echo("Vui lòng điền và kiểm tra lại mã sản phẩm ! ");
         }  
        
    }
?>
<form name="themsanpham" action="" method="post" style="background-color: white">
    <table width="400px" border="0" align="center" style="margin: 10px auto;">
    <tr>
            <td align="center" style="font-weight: bold;">Mã sản phẩm</td>
            <td><input name="masanpham" id="masanpham" type="text"  /> </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Tên sản phẩm</td>
            <td><input name="tensanpham" id="tensanpham" type="text"  /> </td>
        </tr>
       
        <tr>
            <td align="center" style="font-weight: bold;">Mã loại</td>
            <td>
                
            <select name="loai">
                <?php
					$loai = mysqli_query($conn, "SELECT maloai,tenloai FROM theloai");
					if(mysqli_num_rows($loai)>0){
						while($arrlop=mysqli_fetch_array($loai)){
							if($arrlop['maloai'] == $arrsv['maloai']) $selected='selected="selected"'; else $selected='';
							echo '<option '.$selected.' value="'.$arrlop['maloai'].'">'.$arrlop['tenloai'].'</option>';
						}
					}
				?>
            </select>
            </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Mã cỡ</td>
            <td>
                
            <select name="co">
                <?php
					$co = mysqli_query($conn,"SELECT maco,tenco FROM co");
					if(mysqli_num_rows($co)>0){
						while($arrlop=mysqli_fetch_array($co)){
							if($arrlop['maco'] == $arrsv['maco']) $selected='selected="selected"'; else $selected='';
							echo '<option '.$selected.' value="'.$arrlop['maco'].'">'.$arrlop['tenco'].'</option>';
						}
					}
				?>
            </select>
            </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Mã màu</td>
            <td>
                
            <select name="mau">
                <?php
					$mau = mysqli_query($conn,"SELECT mamau,tenmau FROM mau");
					if(mysqli_num_rows($mau)>0){
						while($arrlop=mysqli_fetch_array($mau)){
							if($arrlop['mamau'] == $arrsv['mamau']) $selected='selected="selected"'; else $selected='';
							echo '<option '.$selected.' value="'.$arrlop['mamau'].'">'.$arrlop['tenmau'].'</option>';
						}
					}
				?>
            </select>
            </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Chất liệu</td>
            <td><input name="chatlieu" type="text"  /> </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Ngày nhập</td>
            <td><input name="ngaynhap" type="date"  /> </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Số lượng</td>
            <td><input name="soluong" type="text"  /> </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Ảnh</td>
            <td><input name="anh" type="file"   /> </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold;">Đơn giá bán</td>
            <td><input name="dongiaban" type="text"  /> </td>
        </tr>

        <tr>
            <td colspan="2" align="center"><input name="submit" type="submit" value="Thêm" /> </td>
        </tr>
    </table>
</form>