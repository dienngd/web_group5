        <?php
        session_start();
        ?>        
        
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <a href="trang_quan_tri.php" class="site_title"><i class="fa fa-wrench"></i> <span>QUẢN TRỊ</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/admin1.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Chào mừng,</span>
                <h2><?php echo $_SESSION['email']?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
              <h3>Danh mục</h3>
                <ul class="nav side-menu">
                  <li><a href="./trang_quan_tri.php"><i class="fa fa-home"></i> Trang quản trị</a>
                  </li>
                  
                  
                  <li><a href="./quan_tri_don_hang.php"><i class="fa fa-shopping-cart"></i> Quản trị Đơn hàng</a>
                
                  </li>
            
                  <li><a><i class="fa fa-list-alt"></i> Quản trị Sản phẩm <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="./themsanpham.php">Đăng sản phẩm mới</a></li>
                      <li><a href="./sanpham.php">Cập nhật Sản phẩm</a></li>
                    </ul>
                      <li><a href="./quan_tri_lien_he.php"><i class="fa fa-envelope-o"></i> Quản trị Liên hệ</a>
                  </li>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
          
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/admin1.png" alt="">Admin
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="../controller/c_dangnhap.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->