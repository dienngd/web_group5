﻿
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Trang quản trị</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <?php
       include("top.php");
        ;?>

<?php
    include("connect.php");
?>
<script type="text/javascript">
	function deleleAction(){
		return confirm("Bạn có muốn xóa sản phẩm này không?");
	}
</script>
<div id="left">
	<fieldset>
		<legend>Thông tin sản phẩm</legend>
		<table width="800px" height="500px" border="0" style="margin-left: 30%; background-color: white">
			<tr>
				<td width = "150" align="center" style="font-weight: bold;">Mã sản phẩm</td>
				<td width = "300" align="center" style="font-weight: bold;">Tên sản phẩm</td>
				<td width = "100" align="center" style="font-weight: bold;">Mã loại</td>
				<td width = "100" align="center" style="font-weight: bold;">Mã cỡ</td>
				<td width = "100" align="center" style="font-weight: bold;">Mã màu</td>
				<td width = "200" align="center" style="font-weight: bold;">Chất liệu</td>
				<td width = "300" align="center" style="font-weight: bold;">Ngày nhập</td>
				<td width = "100" align="center" style="font-weight: bold;">Số lượng</td>
				<td width = "200" align="center" style="font-weight: bold; width:10px; height: 20px">Ảnh</td>
				<td width = "300" align="center" style="font-weight: bold;">Đơn giá bán</td>
                <td width = "100" align="center" style="font-weight: bold;">Sửa</td>
                <td width = "100" align="center" style="font-weight: bold;">Xóa</td>
			</tr>
            <?php

                $sql = mysqli_query($conn,"SELECT * FROM sanpham ");
				if(mysqli_num_rows($sql)>0){
                    while($row = mysqli_fetch_array($sql)){
                        $masanpham = $row['masanpham'];
                        $tensanpham= $row['tensanpham'];
                        $maloai = $row['maloai'];
                        $maco = $row['maco'];
                        $mamau = $row['mamau'];
                        $chatlieu = $row['chatlieu'];
                        $ngaynhap = $row['ngaynhap'];
                        $soluong = $row['soluong'];
                        $anh = $row['anh'];
                        $dongiaban = $row['dongiaban'];

                        echo "<tr>
                            <td>$masanpham</td>
                            <td>$tensanpham</td>
                            <td>$maloai</td>
                            <td>$maco</td>
                            <td>$mamau</td>
                            <td>$chatlieu</td>
                            <td>$ngaynhap</td>
                            <td>$soluong</td>
                            <td><img src='$anh' style='width: 100px; height: 200px;'/></td>
                            <td>$dongiaban</td>
                            <td><a href='suasanpham.php?masanpham=".$masanpham."'>Sửa</a> </td>
                            <td><a href='index.php?page=xoasanpham&masanpham=".$masanpham."' onclick='return deleleAction();'>Xóa</a></td>
                        </tr>";
                    }
                }
            ?>
		</table>
	</fieldset>
</div>

 <?php 
          include("bottom.php");
        ;?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
	
  </body>
</html>
