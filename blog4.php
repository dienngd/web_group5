<html>
<head>
    <title>Blog</title>
    <link rel="icon" href="./images/icon2.png" type="images/x-icon"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
<style>
* {
    box-sizing: border-box;
}


/* Middle column */
.column.middle {
    border: 1px solid blanchedalmond;
    width: 80%;
    margin: auto;
    text-align: justify;
    padding-bottom: 2cm;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}
.p1 {
    text-align: center;
}
img.cangiua {display: block; margin-left: auto; margin-right: auto;} 


/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
    .column.side, .column.middle {
        width: 100%;
    }
}
</style>
</head>
<body>
<?php include "banner.php";?>

<br>
<br>

        <?php include "menu.php";?>
        
<br>
    <?php include "anhdong.php";?>
<br>

        <div class="column middle">
                <h1>BÍ KÍP PHỐI ĐỒ KHI DU LỊCH ĐÀ LẠT CHO NÀNG THA HỒ SỐNG ẢO</h1>
                <p>Đầu năm mới là thời điểm cực phù hợp để nàng “xách ba lô lên và đi”. Đây là mùa lễ hội lớn nhất trong năm, đặc biệt đây là mùa mà Đà Lạt duyên dáng nhất, nên thơ nhất và là thỏi nam châm thu hút lòng người nhất.
                </p>
                <p> Nếu có dự định đến thành phố này, đừng bỏ qua những gợi ý trang phục của 92wear nhé.</p>
               
                <h1> Kết hợp áo phông với chân váy</h1>
                <p> Trang phục đi du lịch Đà Lạt không thể bỏ qua những chiếc áo khoác dáng dài bởi độ chất và ‘ ăn ảnh’. Những chiếc áo khoác dáng dài, đặc biệt với chất liệu dạ khá hot trong thời gian gần đây là một món đồ các nàng không nên làm lơ khi soạn tủ đồ của mình. Để an toàn và có thể phối được với nhiều quần áo khác, nàng nên chọn những loại áo khoác có thiết kế đơn giản, màu sắc tối như đen, nâu, xanh navy hoặc xám. Độ dài và ôm của những chiếc áo khoác bạn cũng cần phải lưu tâm để tránh trở nên luộm thuộm, kém duyên. Đối với những nàng có vóc dáng tròn, hãy chọn những loại áo khoác dáng dài có dây thắt ngang eo. Còn những nàng ‘nấm lùn’ hãy sử dụng áo có độ dài vừa phải.</p>
            <div class = "p1">
                <img src = "images\Phương58.jpg" class="cangiua" width="70%" >
                </div>
    
                <div class = "p1">
  <img src = "images\Phương59.jpg" class="cangiua" width="70%"> </div>
  <div class = "p1">
  <img src = "images\Phương60.jpg" class="cangiua" width="70%" > 
  
  <div class = "p1">
  <img src = "images\Phương61.jpg" class="cangiua" width="70%" ></div>
  <div class = "p1">
    
  <img src = "images\Phương62.jpg" class="cangiua" width="70%"  ></div>
  </div>
 
        <h2> Áo len</h2>
        <p> Áo len là thứ mà ai cũng sẽ xếp bỏ vào vali đầu tiên khi chuẩn bị trang phục đi du lịch Đà Lạt. Những chiếc áo len với nhiều kiểu dáng, họa tiết đã trở thành món đồ ‘bất li thân’ khi đến xứ lạnh.</p>

                <p>Nàng có thể chọn áo len oversize để làm tăng vẻ cá tính cho trang phục hoặc cardigan len để set đồ them nữ tính, điệu đà.</p>
                
                <p>Điều quan trọng nhất vẫn là chất vải dày dặn để giúp bạn bảo vệ sức khỏe để tận hưởng trọn vẹn chuyến đi.</p>
    <div class = "p1">
        <img src = "images\Phương63.jpg" class="cangiua" width="70%" >
        </div>
  
  <div class ="p1">
  <img src = "images\Phương64.jpg" class="cangiua" width="70%" ></div>
  <div class ="p1">
        <img src = "images\Phương65.jpg" class="cangiua" width="70%" ></div>
    <div class ="p1">
                <img src = "images\Phương66.jpg" class="cangiua" width="70%" ></div>
                <div class ="p1">
                <img src = "images\Phương67.jpg" class="cangiua" width="70%" ></div>
                
      

  <h2> Denim jacket</h2>
  <p> Denim jacket đã quá quen thuộc với các cô nàng, bởi chúng cũng giống như hơi thở của cuộc sống, hiếm khi có một loại áo nào có bề dày lịch sử như item này.  Denim jacket không kén người mặc như mọi người vẫn nghĩ, vì chúng được thiết kế với rất nhiều kiểu dáng. Với những cô nàng có thân hình hơi tròn, hãy chọn những chiếc áo hơi ôm, tạo cảm giác lưng dài hơn, thon gọn hơn. Denim jacket khi kết hợp cùng chiếc áo len cổ lọ ôm sát cơ thể sẽ khiến bạn trở thành một fashionisto thực thụ giữa lòng thành phố mộng mơ.</p>

         
<div class = "p1">
  <img src = "images\Phương69.jpg" class="cangiua" width="70%" >
  </div>

<div class ="p1">
<img src = "images\Phương70.jpg" class="cangiua" width="70%" ></div>
<div class ="p1">
  <img src = "images\Phương71.jpg" class="cangiua" width="70%" ></div>
<div class ="p1">
          <img src = "images\Phương72.jpg" class="cangiua" width="70%" ></div>
         
        



  
</div>
  
</body>
</html>
