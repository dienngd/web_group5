<?php
class m_combobox
{
    function m_combobox()
    {
        include_once("m_database.php");        
    }
    
    function selectAllMau()
    {
        $con = new database();
        $sql = "SELECT * FROM mau";
        $items = $con->select_all_query($sql);
		echo json_encode($items); /*format về dạng json string đẩy xuống client xử lý*/
    }
	
    function selectAllCo()
    {		
        $con = new database();
        $sql = "SELECT * FROM co";
        $items = $con->select_all_query($sql);
		echo json_encode($items); /*format về dạng json string đẩy xuống client xử lý*/
    }
	
    function selectAllLoai()
    {
        $con = new database();
        $sql = "SELECT * FROM theloai";
        $items = $con->select_all_query($sql);
		echo json_encode($items); /*format về dạng json string đẩy xuống client xử lý*/
    }
	
    function selectAllChatLieu()
    {		
        $con = new database();
        $sql = "SELECT * FROM chatlieu";
        $items = $con->select_all_query($sql);
		echo json_encode($items); /*format về dạng json string đẩy xuống client xử lý*/
    }
	
}


?>