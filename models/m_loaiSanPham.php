<?php
class m_loaiSanPham
{
    function m_loaiSanPham()
    {
        include_once("m_database.php");        
    }
    
    function selectAllLoaiSanPham()
    {
		$result = array();
		
        $con = new database();
        $sql = "SELECT * FROM theloai";
        $items = $con->select_all_query($sql);
		
		$result["rows"]= $items;  /*Định dạng dữ liệu cho easyui datagrid ->rows = jsonArray */
		echo json_encode($result); /*format về dạng json string đẩy xuống client xử lý*/
    }
	
	function addLoaiSanPham($maloaisp=null, $tenloaisp=null)
    {
		$con = new database();
        $sql = "INSERT INTO theloai(`maloai`,`tenloai`) values(";
        $sql .= "'".$maloaisp."',";
        $sql .= "'".$tenloaisp."')";
        $result = $con->execute_query($sql);
		
		if ($result){
			echo json_encode(array(
				'maloai' => $maloaisp,
				'tenloai' => $tenloaisp
			));
		} else {
			echo json_encode(array('errorMsg'=>'Có lỗi xảy ra.'));
		}
    }
	
	function editLoaiSanPham($maloaisp=null, $tenloaisp=null)
    {
		$con = new database();
        $sql = "UPDATE theloai SET TENLOAI = ";
        $sql .= "'".$tenloaisp."'";
        $sql .= " WHERE MALOAI = '".$maloaisp."'";
        $result = $con->execute_query($sql);
		
		if ($result){
			echo json_encode(array(
				'maloai' => $maloaisp,
				'tenloai' => $tenloaisp
			));
		} else {
			echo json_encode(array('errorMsg'=>'Có lỗi xảy ra.'));
		}
    }
	
	function delLoaiSanPham($maloaisp=null)
    {
		$con = new database();
        $sql =  "DELETE FROM THELOAI ";
        $sql .= "WHERE MALOAI = '".$maloaisp."'";
        $result = $con->execute_query($sql);
		
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('errorMsg'=>'Có lỗi xảy ra.'));
		}
    }
}


?>