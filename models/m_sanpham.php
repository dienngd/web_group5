<?php
class m_sanpham
{
    function m_sanpham()
    {
        include_once("m_database.php");        
    }
    
    function selectAllProductTypes()
    {
        $con = new database();
        $sql = "SELECT * FROM sanpham";
        $result = $con->select_all_query($sql);
        return $result;
    }
    
	function selectAllSanPham()
    {
		$result = array();
		
        $con = new database();
        $sql = "SELECT masanpham,tensanpham,maloai,maco,mamau,chatlieu,
					    DATE_FORMAT(ngaynhap,'%d/%m/%Y') AS ngaynhap ,soluong,anh,dongiaban FROM sanpham ";
        $items = $con->select_all_query($sql);
		
		$result["rows"]= $items;  /*Định dạng dữ liệu cho easyui datagrid ->rows = jsonArray */
		echo json_encode($result); /*format về dạng json string đẩy xuống client xử lý*/
    }
	
	function selectOneSanPham($masanpham=null)
    {
		$result = array();
		
        $con = new database();
        $sql = "SELECT masanpham,tensanpham,maloai,maco,mamau,chatlieu,
					    DATE_FORMAT(ngaynhap,'%d/%m/%Y') AS ngaynhap ,soluong,anh,dongiaban 
				FROM sanpham ";
        $sql .="WHERE masanpham = '".$masanpham."'";		
				 
        $items = $con->select_query($sql);
		return $items; 
    }
	function addSanPham($masanpham=null, $tensanpham=null, $maloai=null, $maco=null,$mamau=null,$chatlieu=null,$ngaynhap=null,$soluong=null,$anh=null,$dongiaban=null)
    {
		$con = new database();
        $sql = "INSERT INTO sanpham(`masanpham`,`tensanpham`,`maloai`,`maco`,`mamau`,`chatlieu`,`ngaynhap`,`soluong`,`anh`,`dongiaban`) values(";
        $sql .= "'".$masanpham."',";
		$sql .= "'".$tensanpham."',";
        $sql .= "'".$maloai."',";
        $sql .= "'".$maco."',";
        $sql .= "'".$mamau."',";
        $sql .= "'".$chatlieu."',";
        $sql .= "STR_TO_DATE('".$ngaynhap."', '%d/%m/%Y'),";
        $sql .= "'".$soluong."',";
        $sql .= "'".$anh."',";
		$sql .= "'".$dongiaban."')";
        $result = $con->execute_query($sql);
		
		if ($result){
			echo json_encode(array(
				'masanpham' => $masanpham,
				'tensanpham' => $tensanpham,
				'maloai' => $maloai,
				'maco' => $maco,
				'mamau' => $mamau,
				'chatlieu' => $chatlieu,
				'ngaynhap' => $ngaynhap,
				'soluong' => $soluong,
				'anh' => $anh,
				'dongiaban' => $dongiaban,
			));
		} else {
			echo json_encode(array('errorMsg'=>'Có lỗi xảy ra.'));
		}
    }
	
	function editSanPham($masanpham=null, $tensanpham=null, $maloai=null, $maco=null,$mamau=null,$chatlieu=null,$ngaynhap=null,$soluong=null,$anh=null,$dongiaban=null)
    {
		$con = new database();
        $sql = "UPDATE sanpham SET ";
        $sql .= " tensanpham = '".$tensanpham."', ";
		$sql .= " maloai = '".$maloai."', ";
        $sql .= " maco = '".$maco."', ";
        $sql .= " mamau = '".$mamau."', ";
        $sql .= " chatlieu = '".$chatlieu."', ";   
        $sql .= " ngaynhap = STR_TO_DATE('".$ngaynhap."', '%d/%m/%Y'), ";
        $sql .= " soluong = '".$soluong."', ";
		if($anh <> null and $anh <> '' ){
		    $sql .= " anh = '".$anh."', ";
		}
        $sql .= " dongiaban = '".$dongiaban."' ";
        $sql .= "WHERE masanpham = '".$masanpham."'";		
        $result = $con->execute_query($sql);
		if ($result){
			echo json_encode(array(
				'maloai' => $masanpham,
				'tenloai' => $tensanpham
			));
		} else {
			echo json_encode(array('errorMsg'=>'Có lỗi xảy ra.'));
		}
    }
	
	function delSanPham($masanpham=null)
    {
		$con = new database();
        $sql =  "DELETE FROM sanpham ";
        $sql .= "WHERE masanpham = '".$masanpham."'";
        $result = $con->execute_query($sql);
		
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('errorMsg'=>'Có lỗi xảy ra.'));
		}
    }
    function selectOneProductTypes($id)
    {
        $con = new database();
        $sql = "SELECT * FROM mis_product_types WHERE id=".$id;
        $result = $con->select_query($sql);
        return $result;
    }
    
    function insertProductType($name=null, $description=null, $image=null)
    {
        $con = new database();
        $sql = "INSERT INTO mis_product_types(`name`,`description`, `image`) values(";
        $sql .= "'".$name."',";
        $sql .= "'".$description."',";
        $sql .= "'".$image."')";
        $result = $con->execute_query($sql);
        return $result;
    }
    
    function updateProductType($id, $name=null, $description=null, $image=null)
    {
        $con = new database();
        $sql = "UPDATE mis_product_types SET ";
        $sql .= "`name` = '".$name."',";
        $sql .= "`description` = '".$description."',";
        $sql .= "`image` = '".$image."'";
        $sql .= " WHERE id =".$id;

        $result = $con->execute_query($sql);
        return $result;
    }
    
    function deleteProductType($id)
    {
        $con = new database();
        $sql = "DELETE FROM mis_product_types WHERE id = ".$id;
        $result = $con->execute_query($sql);
        return $result;
    }
    
    function selectAllProduct($productTypeId = null)
    {
        $con = new database();
        $sql = "SELECT * FROM mis_products WHERE 1";
        if($productTypeId != null)
        {
            $sql .= " AND productTypeId = ". $productTypeId;
        }
        $result = $con->select_all_query($sql);   
        return $result;
    }
    
    function selectOneProduct($productId)
    {
        $con = new database();      
        $sql = "SELECT * FROM mis_products WHERE id = ".$productId;
        $result = $con->select_query($sql);        
        return $result;
    }
    
    function insertProduct($name=null, $description=null, $image=null, $price=null, $quantity=null, $productTypeId)
    {
        $con = new database();
        @session_start();
        $sql = "INSERT INTO mis_products(`name`,`description`, `image`, `price`, `quantity`, `productTypeId`, `createdBy`) values(";
        $sql .= "'".$name."',";
        $sql .= "'".$description."',";
        $sql .= "'".$image."',";
        $sql .= "'".$price."',";
        $sql .= "'".$quantity."',";
        $sql .= "'".$productTypeId."',";
        $sql .= "'".$_SESSION['userId']."')";
        print_r($sql);
        $result = $con->execute_query($sql);
        return $result;
    }
    
    function updateProduct($id, $name=null, $description=null, $image=null, $price=null, $quantity=null, $productTypeId)
    {
        $con = new database();
        @session_start();
        $sql = "UPDATE mis_products SET ";
        $sql .= "`name` = '".$name."',";
        $sql .= "`description` = '".$description."',";
        $sql .= "`price` = '".$price."',";
        $sql .= "`quantity` = '".$quantity."',";
        $sql .= "`lastModifiedBy` = '".$_SESSION['userId']."',";
        $sql .= "`productTypeId` = '".$productTypeId."',";
        $sql .= "`image` = '".$image."'";
        $sql .= " WHERE id =".$id;

        $result = $con->execute_query($sql);
        return $result;
    }
    
    function deleteProduct($id)
    {
        $con = new database();
        $sql = "DELETE FROM mis_products WHERE id = ".$id;
        $result = $con->execute_query($sql);
        return $result;
    }
    
    function addToCart($product = array())
    {
        @session_start();
        if($_SESSION['login'] == 1)
        {
            $_SESSION['cart']['products'][] = array(
                'id' => $product['id'],
                'name' => $product['name'],
                'price' => $product['price'],
                'add-quantity' => $product['add-quantity']
            );
            $_SESSION['cart']['buying-quantities'] += $product['add-quantity'];
            
            return true;
        }
        else
            return false;
    }
    
    function insertInvoice()
    {
        $con = new database();
        @session_start();
        if($_SESSION['cart']['buying-quantities'] > 0)
        {
            $invoiceCode = $_SESSION['userId']."-".time();
            $sql = "INSERT INTO mis_invoices(code, createdBy) VALUES(";
            $sql .= "'".$invoiceCode."',";
            $sql .= "'".$_SESSION['userId']."')";            
            
            if($con->execute_query($sql))
            {
                //Lấy ID của invoice vừa tạo:
                $sql1 = "SELECT id FROM mis_invoices WHERE code = '".$invoiceCode."'";
                $invoice = $con->select_query($sql1);
                
                //Tạo các chi tiết đơn hàng:
                $sql2 = "INSERT INTO mis_invoice_details(invoiceId, productId, quantity, price, createdBy) VALUES";
                $i = 0;
                foreach($_SESSION['cart']['products'] as $cart)
                {
                    $i++;
                    $sql2 .= "('".$invoice['id']."','".$cart['id']."','".$cart['add-quantity']."','".$cart['price']."','".$_SESSION['userId']."')";
                    if($i < sizeof($_SESSION['cart']['products']))
                    {
                        $sql2 .= ",";
                    }
                }
                if($con->execute_query($sql2))
                {
                    //Nếu thêm đơn hàng thành công thì phải hủy các session lưu đơn hàng tạm thời đi.
                    unset($_SESSION['cart']);
                    $_SESSION['cart']['products'] = array();
                    $_SESSION['cart']['buying-quantities'] = 0;
                    return true;
                }
                else return false;
                
                
            }
            
        }
    }
    
    function selectInvoiceByUser($userId)
    {
        $con = new database();
        $sql = "SELECT mis_invoices.*, sum(mis_invoice_details.price*mis_invoice_details.quantity) as total
        FROM mis_invoices INNER JOIN mis_invoice_details ON 
        mis_invoices.id = mis_invoice_details.invoiceId 
        WHERE mis_invoices.createdBy = ".$userId." GROUP BY mis_invoice_details.invoiceId";

        $result = $con->select_all_query($sql);
        return $result;

    }
    
    function selectInvoiceDetailByInvoice($invoiceId)
    {
        $con = new database();
        $sql = "SELECT mis_invoice_details.*, mis_products.name FROM mis_invoice_details
        INNER JOIN mis_products ON mis_invoice_details.productId = mis_products.id 
         WHERE invoiceId = ".$invoiceId;

        $result = $con->select_all_query($sql);
        return $result;

    }
    

}


?>