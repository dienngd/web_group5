<!DOCTYPE html>
<?php
  session_start();
?>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thông tin đơn hàng</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="./images/icon2.png" type="images/x-icon"/>
    <link rel="stylesheet" type="text/css" href=".\css\style.css">
    <script src="main.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<?php include "banner.php";?>

<br>
<br>
<?php include "menu.php";?>
<br>
<?php include "anhdong.php";?>

    <div id="phai" style="width: 95%;margin:center; float:left">
        <div id="nhaptt" style="margin-left:5%;float:left;width: 45%"> 
            <form method="post" action="hoa_don_them_moi.php" >
                <h3>Bạn đang cần mua hàng. Vui lòng điền thông tin! </h3>
                <p>(Những mục có dấu sao * là bắt buộc)</p>
                <p><span id="do_rong" >Họ và tên *:</span><input placeholder="(Ghi đầy đủ họ tên)" type=”text” name="hoten" required></p>
                <p><span id="do_rong" >Số điện thoại *:</span><input type=”text” name="sdt" required></p>
                <p><span id="do_rong">Ngày mua:</span><input name="ngaymua" readonly = "readonly" value="<?php echo date('y/m/d');?>">
                <p><span id="do_rong">Tổng tiền: </span><input readonly = "readonly" name="tongtien" value="<?=($_SESSION['gio_hang']['tong_tien'])?>">VNĐ</p>
                <p><span id="do_rong">Ngày giao *:</span> <input type="date" name="ngaygiao" <?= date("d.m.y")?> required></p>
                <p><span id="do_rong">Địa chỉ *:</span> <textarea type="text" style="width: 400px;height: 60px;" name="diachi" required></textarea></p>
                <p><span id="do_rong">Ghi chú (nếu có):</span> <textarea type="text" style="width: 400px;height: 80px;" name="ghichu"></textarea></p>

            </div>

            <div  id="nhaptt1" style="float:left;width: 50%;float:left">
        <h3 style="text-align: center;">ĐƠN HÀNG CỦA BẠN</h3>
        <div class="container">
		<div class="table-responsive cart_info" style="width:95%; margin-left:5%" >
        <table class="table table-condensed" cellpadding="1px" cellspacing="1px" >
			<thead>
			<tr class="cart_menu" style="background-color: pink">
          <td class="image" style="width:20%;text-align: center;">Ảnh</td>				
					<td class="description" style="width:32%;text-align: center;">Tên sản phẩm</td>
					<td class="price" style="width:18%;text-align: center;">Giá</td>
					<td class="quantity" style="width:12%;text-align: center;">Số lượng</td>
					<td class="total" style="width:20%;text-align: center;">Thành tiền</td>
			</tr>		
			</thead>
            <?php       
                    
                    if(isset($_SESSION['gio_hang']) && $_SESSION['gio_hang']['tong_so'] > 0){
                   
                        $tongTien=0;
                        foreach($_SESSION['gio_hang']['mat_hang'] as $matHang =>$value){
                            $thanhTien = $value['so_luong'] * $value['dongiaban'];
                            $tongTien += $thanhTien;
            ?>        
			<tbody>
				<tr>  
        <td class="cart_product"  style="text-align: center;" >
        <img src="<?=$value['anh']?>" alt="" style="width: 100%; height:100%; " />
				</td>
					
					<td class="cart_description" style="text-align: center;">
					<h3 style="font-size:16px;color:#6E4D8B;"><?=$value['tensanpham']?></h3>
					</td>

					<td class="cart_price" style="text-align: center;margin-top:10px;">
						<p style="font-size:16px;color:#6E4D8B;"><?=$value['dongiaban']?> VNĐ</p>
					</td>

					<td class="cart_quantity" style="text-align: center">
              <p style="font-size:16px;color:#6E4D8B;"><?=$value['so_luong']?></p>					
					</td>
			
					<td class="cart_total">
						<p class="cart_total_price" style="text-align: center;color:#6E4D8B;font-size:16px"><?=$thanhTien?> VNĐ</p>
					</td>

				</tr>
                    <?php }}
                    $_SESSION['gio_hang']['tong_tien']=$tongTien; ?>
				<tr>
                    <td style="font-size:20px" colspan="7" class="text-left">
                    Tổng tiền: <strong style="font-size: 20px;color:#6E4D8B" class="text-primary"><?=$tongTien?> VNĐ</strong>
                    </td>
                </tr>
    </table>
    <style type="text/css">
   table, #cart_items .cart_info {
    border: 3px solid rgb(8, 7, 4);
    margin-bottom: 50px;

  }
    
   .cart_menu {
    background: rgb(120, 82, 148);
    color: #fff;
    font-size: 16px;
    font-family: 'Roboto', sans-serif;
    font-weight: normal;
    text-align: center;
  }
  
  .cart_info .table.table-condensed thead tr {
    height: 40px;
  }
  
  th {
    border-top:1px solid gray;
    border-bottom: 1px solid gray;
}
td {
    border-bottom: 1px solid gray;
}

 .cart_info .image {
    padding-left: 30px;
  }
  
   .cart_info .cart_description h4 {
    margin-bottom: 0;
  }
  
   .cart_info .cart_description h4 a {
    color: #363432;
    font-family: 'Roboto',sans-serif;
    font-size: 20px;
    font-weight: normal;
  }
  
  .cart_info .cart_description p {
    color:#696763;
  }
  
   .cart_info .cart_price p {
    color:#17151a;
    font-size: 18px;
  }
  
   .cart_info .cart_total_price {
    color: rgb(196, 13, 38);
    font-size: 20px;
  }
  .cart_product {
    display: block;
    margin: 5px -0px 0px 0px;
  }
  
  .cart_quantity_button a {
    background:#F0F0E9;
    color: #696763;
    display: inline-block;
    font-size: 16px;
    height: 28px;
    overflow: hidden;
    text-align: center;
    width: 35px;
    float: left;
  }
  
  .cart_quantity_input {
    color: #696763;
    float: left;
    font-size: 16px;
    text-align: center;
    font-family: 'Roboto',sans-serif;
  }

 
   </style>
    </div></div>
   
</div></div>

                <div align=right><input name="xacnhan" style="background-color: pink;color: white; width: 110px;height: 30px;font-size: 14pt;margin-right:60px" type="submit" id="xacnhan" value="Xác nhận"/> </div><br><br>
           </form>  
</div>           
<br><br>
<?php include "footer.php" ;?>
       
</body>
</html>