<!DOCTYPE html>
<html lang="en">
<?php
require("header.php");
?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include 'leftmenu.php';?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
			<h4><i class="fa fa-edit"></i> Quản lý loại sản phẩm </h4>
      </ol>
      <!-- Example DataTables Card-->
	  <div class="form-group">
			<table  id="dg" class="easyui-datagrid" style="height:550px"
					url="/Nhom5/controller/c_loaiSanPham.php?task=view"
					toolbar="#toolbar"
					rownumbers="true" fitColumns="true" singleSelect="true">
				<thead>
					<tr>
						<th field="maloai" width="50">Mã sản loại phẩm</th>
						<th field="tenloai" width="50">Tên loại sản phẩm</th>
					</tr>
				</thead>
			</table>
			<div id="toolbar">
				<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newLoaiSP()">Thêm</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editLoaiSP()">Sửa</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyLoaiSP()">Xóa</a>
			</div>
    
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- add dgl-->
	<div id="dlg" class="easyui-dialog" style="width:360px;height:300px;padding:10px 20px"
        closed="true" buttons="#dlg-buttons">
    <div class="ftitle">Thông tin loại sản phẩm</div>
    <form id="fm" method="post" novalidate>
		<div class="row" style='overflow-x: hidden;'>		
			<div class="fitem">
				<label>Mã loại sản phẩm:</label>
				<input id = "maloai" name="maloai" class="easyui-textbox" required="true">
			</div>
			<div class="fitem">
				<label>Tên loại sản phẩm:</label>
				<input name="tenloai" class="easyui-textbox" required="true">
			</div>
		</div>
    </form>
	</div>
	<div id="dlg-buttons"> 
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveLoaiSP()" style="width:90px">Lưu</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Hủy</a>
	</div>
    <!-- Bootstrap core JavaScript-->
    <script src="/Nhom5/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/Nhom5/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/Nhom5/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/Nhom5/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/Nhom5/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/Nhom5/js/sb-admin-datatables.min.js"></script>
	<script type="text/javascript">
		var url;
		function newLoaiSP(){
			$('#dlg').dialog('open').dialog('setTitle','Thêm mới');
			$('#fm').form('clear');
		    $('#fm #maloai').attr('readonly', false);
			url = '/Nhom5/controller/c_loaiSanPham.php?task=add';
		}
		function editLoaiSP(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','Sửa');
				$('#fm').form('load',row);
				 $('#maloai').textbox({disabled: true});

				url = '/Nhom5/controller/c_loaiSanPham.php?task=edit';
			}
		}
		function saveLoaiSP(){
			$('#maloai').textbox({disabled: false});
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						//$('#dlg').dialog('close');		// Đóng dialog
						$('#dg').datagrid('reload');	// Load lại datagrid
					}
				}
			});
		}
		function destroyLoaiSP(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Xác nhận','Bạn muốn xóa loại sản phẩm này?',function(r){
					if (r){
						$.post(                                             //$.post có tác dụng lấy dữ liệu từ server bằng phương thức HTTP POST REQUEST 
							'/Nhom5/controller/c_loaiSanPham.php?task=del',  //url
							{maloai:row.maloai},                            //post data json string
							function(result){                               //Hàm xử lý khi thành công 
								if (result.success){
									$('#dg').datagrid('reload');	        //Load datagrid
								} else {
									$.messager.show({	// Bắn lỗi
										title: 'Error',
										msg: result.errorMsg
									});
								}
							},'json'     	                                //Loại dữ liệu trả về
						);
					}
				});
			}
		}
	</script>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
  </div>
</body>

</html>
