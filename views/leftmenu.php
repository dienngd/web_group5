<!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="/Nhom5/thong_tin_dang_nhap123.html">MY PRODUCTS REVIEW</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Quản lý loại sản phẩm">
          <a class="nav-link" href="/Nhom5/views/v_loaiSP.php">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text"> Quản lý loại sản phẩm</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Quản lý sản phẩm">
          <a class="nav-link" href="/Nhom5/views/v_sanPham.php">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text"> Quản lý sản phẩm</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="/Nhom5/Trangchu.html">
            <i class="fa fa-fw fa-sign-out"></i>Trang chủ</a>
        </li>
      </ul>
    </div>
  </nav>