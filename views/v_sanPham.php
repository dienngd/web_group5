<!DOCTYPE html>
<html lang="en">
<?php
require("header.php");
?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include 'leftmenu.php';?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
	  <div  col-xs-12>
		  <ol class="breadcrumb">
				<h4><i class="fa fa-edit"></i> Quản lý sản phẩm</h4>
		  </ol>
	  </div>
      <!-- Example DataTables Card-->
			<div>
				<table  id="dg" class="easyui-datagrid" style="height:550px"
						url="/Nhom5/controller/c_sanPham.php?task=view"
						toolbar="#toolbar"
						rownumbers="true" fitColumns="true" singleSelect="true">
					<thead>
						<tr>
							<th field="masanpham" width="80">Mã sản phẩm</th>
							<th field="tensanpham" width="80">Tên sản phẩm</th>
							<th field="maloai" width="50">Mã loại</th>
							<th field="maco" width="50">Mã cỡ</th>
							<th field="mamau" width="50">Mã Màu</th>
							<th field="chatlieu" width="50">Chất liệu</th>
							<th field="ngaynhap" width="60">Ngày nhập</th>
							<th field="soluong" width="50">Số lượng</th>
							<th field="dongiaban" width="70">Đơn giá bán</th>
							<th field="anh" width="50">Ảnh</th>
						</tr>
					</thead>
				</table>
				<div id="toolbar">
					<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newSanPham()">Thêm</a>
					<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editSanPham()">Sửa</a>
					<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="delSanPham()">Xóa</a>
				</div>
			</div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- add dgl-->
	<div id="dlg" class="easyui-dialog" style="width:800px;height:600px;padding:10px 20px"
        closed="true" buttons="#dlg-buttons">
    <div class="ftitle">Thông tin sản phẩm</div>
    <form id="fm" method="post" novalidate >
			<div style="margin-bottom:20px">
				<input id="masanpham" name="masanpham" style="width:48%;" class="easyui-textbox" required="true" data-options="label:'Mã sản phẩm:'">
				<input id="tensanpham" name="tensanpham" style="width:48%;" class="easyui-textbox" required="true" data-options="label:'Têm sản phẩm:'">
			</div>
			<div style="margin-bottom:20px">
				<input id ="maloai" class="easyui-combobox" name="maloai" style="width:48%;" data-options="
                    url:'/Nhom5/controller/c_combobox.php?option=loai',
                    method:'get',
                    valueField:'maloai',
                    textField:'tenloai',
                    panelHeight:'auto',
                    label: 'Mã loại'
                    ">		
				
				<input id ="maco" class="easyui-combobox" name="maco" style="width:48%;" data-options="
                    url:'/Nhom5/controller/c_combobox.php?option=co',
                    method:'get',
                    valueField:'maco',
                    textField:'tenco',
                    panelHeight:'auto',
                    label: 'Mã cỡ'
                    ">					
			</div>
			<div style="margin-bottom:20px">
				<input id ="mamau" class="easyui-combobox" name="mamau" style="width:48%;" data-options="
                    url:'/Nhom5/controller/c_combobox.php?option=mau',
                    method:'get',
                    valueField:'mamau',
                    textField:'tenmau',
                    panelHeight:'auto',
                    label: 'Mã màu'
                    ">		
				
				<input id ="chatlieu" class="easyui-combobox" name="chatlieu" style="width:48%;" data-options="
                    url:'/Nhom5/controller/c_combobox.php?option=chatlieu',
                    method:'get',
                    valueField:'machatlieu',
                    textField:'tenchatlieu',
                    panelHeight:'auto',
                    label: 'Chất liệu'
                    ">				
			</div>
			<div style="margin-bottom:20px">
				<input id = "ngaynhap" name = "ngaynhap" class="easyui-datebox"  label="Ngày nhập" style="width:48%" data-options="formatter:myformatter,parser:myparser" >
				<input id="soluong" name="soluong" class="easyui-numberbox" label="Số lượng nhập" style="width:48%">
			</div>
			<div style="margin-bottom:20px">
				<input id ="dongiaban" name ="dongiaban" class="easyui-numberbox" label="Đơn giá bán" style="width:48%">
				<input id="anh" value "heheh" name ="anh" class="easyui-filebox" label="Ảnh" style="width:48%" data-options="
				prompt:'Chọn ảnh...',
				onChange: function(value){
					var f = $(this).next().find('input[type=file]')[0];
					if (f.files && f.files[0]){
						var reader = new FileReader();
						reader.onload = function(e){
							$('#image1').attr('src', e.target.result);
						}
						reader.readAsDataURL(f.files[0]);
					}
				}">
			</div>
			<div id="imgContainer" style="margin-bottom:20px;float:right;">
				<img id ="image1" class="w3-border" alt="No IMG" style="padding:16px;200px">
			</div>
    </form>
	</div>
	<div id="dlg-buttons"> 
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveLoaiSP()" style="width:90px">Lưu</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Hủy</a>
	</div>
    <!-- Bootstrap core JavaScript-->
    <script src="/Nhom5/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/Nhom5/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/Nhom5/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/Nhom5/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/Nhom5/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/Nhom5/js/sb-admin-datatables.min.js"></script>
	<script type="text/javascript">
		var url;
		function newSanPham(){
			$('#dlg').dialog('open').dialog('setTitle','Thêm mới');
			$('img').removeAttr('src');
			$('#fm').form('clear');
				$('#masanpham').textbox({disabled: false});
				enableControl();
			url = '/Nhom5/controller/c_sanPham.php?task=add';			
		}
		function editSanPham(){
			var row = $('#dg').datagrid('getSelected');
			$('#image1').attr('src', "../imgSanPham/"+row.anh)
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','Sửa');
				$('#fm').form('load',row);
				$('#masanpham').textbox({disabled: true});
				enableControl();
				url = '/Nhom5/controller/c_sanPham.php?task=edit';
			}
		}
		function saveLoaiSP(){
			$('#masanpham').textbox({disabled: false});
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlg').dialog('close');		// Đóng dialog
						$('#dg').datagrid('reload');	// Load lại datagrid
					}
				}
			});
		}
		function delSanPham(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Xác nhận','Bạn muốn xóa loại sản phẩm này?',function(r){
					if (r){
						$.post(                                             //$.post có tác dụng lấy dữ liệu từ server bằng phương thức HTTP POST REQUEST 
							'/Nhom5/controller/c_sanPham.php?task=del',  //url
							{masanpham:row.masanpham},                            //post data json string
							function(result){                               //Hàm xử lý khi thành công 
								if (result.success){
									$('#dg').datagrid('reload');	        //Load datagrid
								} else {
									$.messager.show({	// Bắn lỗi
										title: 'Error',
										msg: result.errorMsg
									});
								}
							},'json'     	                                //Loại dữ liệu trả về
						);
					}
				});
			}
		}
        function myformatter(date){								//format String return về dd/mm/yyyy
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
			return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
        }
        function myparser(s){									//parse value return về dd/mm/yyyy
            if (!s) return new Date();
            var ss = (s.split('/'));
            var d = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            var y = parseInt(ss[2],10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                return new Date(y,m-1,d);
            } else {
                return new Date();
            }
        }
		
		function disableControl(){   //Ẩn control khi xem
			$('#tensanpham').textbox({disabled: true});
			$('#maloai').textbox({disabled: true});
			$('#maco').textbox({disabled: true});
			$('#mamau').textbox({disabled: true});
		    $('#chatlieu').textbox({disabled: true});
		    $('#ngaynhap').textbox({disabled: true});
			$('#soluong').textbox({disabled: true});
			$('#anh').textbox({disabled: true});
			$('#dongiaban').textbox({disabled: true});
		}
		
		function enableControl(){    //Hiện control khi thêm sửa
			$('#tensanpham').textbox({disabled: false});
			$('#maloai').textbox({disabled: false});
			$('#maco').textbox({disabled: false});
			$('#mamau').textbox({disabled: false});
		    $('#chatlieu').textbox({disabled: false});
		    $('#ngaynhap').textbox({disabled: false});
			$('#soluong').textbox({disabled: false});
			$('#anh').textbox({disabled: false});
			$('#dongiaban').textbox({disabled: false});
		}
		
	$(document).ready(function(){
		$('#dg').datagrid({
			onDblClickRow: function(){		
				var row = $('#dg').datagrid('getSelected');
				$('#image1').attr('src', "../imgSanPham/"+row.anh)
				if (row){
					$('#dlg').dialog('open').dialog('setTitle','Xem');
					$('#fm').form('load',row);
					$('#masanpham').textbox({disabled: true});
					disableControl();
				}					
			}
		})
	});
	</script>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
		.textbox-label {
			display: inline-block;
			width: 120px;
			height: 30px;
			line-height: 30px;
			vertical-align: middle;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
			margin: 0;
			padding-right: 0
		}
		#imgContainer {
			width: 300px;
			height: 170px;
		}

		#imgContainer img {
			max-width: 100%;
			max-height: 100%;
		}
	</style>
  </div>
</body>

</html>
