<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
        $(document).ready(function() {
            $("#firstname").focus(function() {
                $(".required1").animate({
                    left: '400px',
                    opacity: '0.4',
                }, "fast");
            })
            $("#lastname").focus(function() {
                $(".required2").animate({
                    left: '400px',
                    opacity: '0.4',
                }, "fast");
            })
            $("#email").focus(function() {
                $(".required3").animate({
                    left: '400px',
                    opacity: '0.4',
                }, "fast");
            })
            $("#password").focus(function() {
                $(".required4").animate({
                    left: '400px',
                    opacity: '0.4',
                }, "fast");
            })
            $("#npassword").focus(function() {
                $(".required5").animate({
                    left: '400px',
                    opacity: '0.4',
                }, "fast");
            })
        })
    </script>
    <script>
            function validateForm() {
                var x = document.forms["myForm"]["firstname"].value;
                if (x == "") {
                    alert("BẠN HÃY NHẬP HỌ ");
                    return false;
                }
                var x = document.forms["myForm"]["lastname"].value;
                if (x == "") {
                    alert("BẠN HÃY NHẬP TÊN ");
                    return false;
                }
                var x = document.forms["myForm"]["email"].value;
                if (x == "") {
                    alert("BẠN HÃY NHẬP EMAIL ");
                    return false;
                }
                var x = document.forms["myForm"]["password"].value;
                if (x == "") {
                    alert("BẠN HÃY NHẬP PASSWORD ");
                    return false;
                }
                var x = document.forms["myForm"]["npassword"].value;
                if (x == "") {
                    alert("BẠN HÃY NHẬP LẠI PASSWORD ");
                    return false;
                }
            }
            </script>

<script>
function checkForm()
{
     var firstname = document.forms['dangki']["firstname"].value;
     var lastname = document.forms['dangki']["lastname"].value;
     var password = document.forms['dangki']["password"].value;
     var npassword = document.forms['dangki']["npassword"].value;
     var email = document.forms['dangki']["email"].value;
     
     if(firstname == '')
     {
        alert('Bạn phải nhập đầy đủ thông tin người dùng');
        document.forms["dangki"]["firstname"].focus();
        return false;
     }
     if(lastname == '')
     {
        alert('Bạn phải nhập đầy đủ thông tin người dùng');
        document.forms["dangki"]["lastname"].focus();
        return false;
     }
     else if(password == '')
     {
        alert('Bạn phải nhập mật khẩu');
        document.forms["dangki"]["password"].focus();
        return false;
     }
     else if(email == '')
     {
        alert('Bạn phải nhập email');
        document.forms["dangki"]["email"].focus();
        return false;
     }
     else if(password != npassword)
     {
        alert('Mật khẩu xác nhận chưa khớp !');
        return false;
     }
     else return true;
  
}

</script>

</head>
<body>
<form name="dangki" action="?page=dangki&action=insert" onsubmit="return checkForm()" method="post">
    <h1 align="center"> ĐĂNG KÍ TÀI KHOẢN </h1>
    <table cellpadding="10" cellspacing="1" align="center">
        <tr>
            <td>
                <h2 class="lengend">THÔNG TIN CÁ NHÂN</h2>
            </td>
        </tr>
        <tr>
            <td style="position : relative">
                <label for="firstname" class="required1">Họ<em>*</em></label>

                <input type="text" id="firstname" name="firstname" value title="Họ" maxlength="255" class="input-text required-entry">
            </td>
        </tr>
        <tr>
            <td style="position : relative">
                <label for="lastname" class="required2">Tên<em>*</em></label>
                <input type="text" id="lastname" name="lastname" value="" title="Tên" maxlength="255" class="input-text required-entry">
            </td>
        </tr>
        <tr>
            <td style="position : relative">
                <label for="email_address" class="required3">Địa chỉ Email<em>*</em></label>
                <input type="text" name="email" id="email" value="" title="Địa chỉ email" class="input-text validate-email required-entry">
            </td>
        </tr>
        <tr>

            <td>
                <h2 class="lengend">THÔNG TIN ĐĂNG NHẬP</h2>
            </td>

        </tr>
        <tr>
            <td style="position : relative">
                <label for="password" class="required4">Mật Khẩu<em>*</em></label>
                <input type="password" name="password" value="" id="password" title="Mật khẩu" class="input-text required-entry">
            </td>
        </tr>
        <tr>
            <td style="position : relative">
                <label for="password" class="required5">Nhập Mật Khẩu<em>*</em></label>
                <input type="password" name="npassword" value="" id="npassword" title="hập Lại Mật khẩu" class="input-text required-entry ">
            </td>
        </tr>
        <tr>
            <td>
                <button align="center"> CHẤP NHẬN  </button>
            </td>
        </tr>
    </table>
    </form>
    </body>
    </html>