<html lang=en>
<head>
    <title> ÁO</title>
    <meta charset="utf-8">
    <meta name="viewport" , content="width=device-width, initial-scale=1">
    <script type="text/javascript" language="javascript">
    </script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" href="css/bs4/bootstrap.css">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<?php include_once "./views/layouts/_navbar.php";?>
<?php $ahihi = $sanPham->fetch_assoc() ?>
<div class="container">
    <div class="display-4">Sua Ao</div>
    <form method="post" action="">
        <div class="form-group">
            <label for="name">Tên sản phẩm</label>
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $ahihi['tensanpham'] ?>" placeholder="">
        </div>
        <div class="row">
            <div class="form-group col-lg-4">
                <label for="maloai">Mã loại</label>
                <input type="number" class="form-control" id="maloai" name="maloai" value="<?php echo $ahihi['maloai'] ?>" placeholder="">
            </div>
            <div class="form-group col-lg-4">
                <label for="maco">Mã co</label>
                <input type="number" class="form-control" id="maco" name="maco" value="<?php echo $ahihi['maco'] ?>" placeholder="">
            </div>
            <div class="form-group col-lg-4">
                <label for="mamau">Mã mau</label>
                <input type="number" class="form-control" id="mamau" name="mamau" value="<?php echo $ahihi['mamau'] ?>" placeholder="">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-4">
                <label for="chatlieu">Chat lieu</label>
                <input type="text" class="form-control" id="chatlieu" name="chatlieu" value="<?php echo $ahihi['chatlieu'] ?>" placeholder="">
            </div>
            <div class="form-group col-lg-4">
                <label for="soluong">So luong</label>
                <input type="number" class="form-control" id="soluong" name="soluong" value="<?php echo $ahihi['soluong'] ?>" placeholder="">
            </div>
            <div class="form-group col-lg-4">
                <label for="giaban">Gia ban</label>
                <input type="number" class="form-control" id="giaban" name="giaban" value="<?php echo $ahihi['dongiaban'] ?>" placeholder="">
            </div>
        </div>
        <button type="submit" class="btn btn-primary float-right">Sua</button>
    </form>
</div>
</body>
</html>