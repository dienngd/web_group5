<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style>
table {
    min-width: 500px;
    border: 1px solid lightgrey;
}
label {
    color: #999999;
    font: 500 11px/1.55 Poppins, Helvetica Neue, Verdana, Arial, sans-serif;
    top: 10px;
    right: auto;
    bottom: 0px;
    position: absolute;
    left: 10px;
    -webkit-transition: background 0.2s, color 0.2s, top 0.2s, bottom 0.2s, right 0.2s, left 0.2s;
    -moz-transition: background 0.2s, color 0.2s, top 0.2s, bottom 0.2s, right 0.2s, left 0.2s;
    transition: background 0.2s, color 0.2s, top 0.2s, bottom 0.2s, right 0.2s, left 0.2s;
    line-height: 44px;
    margin: 0;
}
input.input-text {
    border-width: 0;
    border-bottom-width: 1px;
    border-bottom-style: solid;
    border-bottom-color: lightgrey;
    padding-left: 0;
    padding-right: 0;
    font: 500 12px/1.55 PT Sans, Helvetica Neue, Verdana, Arial, sans-serif;
    width: 100%;
    height: 40px !important;
    color: #999999;
}

input.input-text:focus {
    outline-color: transparent;
    border-width: 0;
    border-bottom-width: 1px;
    border-bottom-color: black;
}

.lengend {
    font: 500 14px/1.35 PT Sans, Helvetica Neue, Verdana, Arial, sans-serif;
    color: #282828;
}
</style>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script>
function checkForm()
{
     
     var password = document.forms['dangnhap']["password"].value;
    
     var email = document.forms['dangnhap']["email"].value;
     
     if(email == '')
     {
        alert('Bạn phải nhập email');
        document.forms["dangnhap"]["email"].focus();
        return false;
    
     }else if (password == '')
     {
        alert('Bạn phải nhập mật khẩu');
        document.forms["dangnhap"]["password"].focus();
        return false;
     }
     
     else return true;
  
}

</script>
<script>
    $(document).ready(function() {
        $("#email").focus(function() {
                $(".required3").animate({
                    left: '400px',
                    opacity: '0.4',
                }, "fast");
            })
            $("#password").focus(function() {
                $(".required4").animate({
                    left: '400px',
                    opacity: '0.4',
                }, "fast");
            })
    })



    </script>
</head>
<body>

<form name="dangnhap" action="?page=dangnhap&action=dangnhap" onsubmit="return checkForm()" method="post">
        <h1 align="center"> ĐĂNG NHẬP </h1>
        <table cellpadding="10" cellspacing="1" align="center">
            <tr>
                <td style="position : relative">
                <label for="email_address" class="required3">Địa chỉ Email</label>
                        <input type="text" name="email" id="email" value=""  maxlength="255" maxletitle="Địa chỉ email" class="input-text">
                </td>
            </tr>
            <tr>
                    <td style="position : relative" >
                    <label for="password" class="required4">Mật Khẩu</label>
                            <input type="password" name="password" value="" id="password" maxlength="255" title="Mật khẩu" class="input-text ">
                        </td>
            </tr>
            <tr>
                <td style="position : relative">
                    <button align="center"> ĐĂNG NHẬP </button>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="">Quên mật khẩu?</a>
                </td>
            </tr>
        </table>
            </form>
            </body>
            </html>

