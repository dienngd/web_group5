<?php
	include("../models/m_sanpham.php");
	$models = new m_sanpham();
	$task = $_GET['task'];
	
	if($task)    
	{
		switch($task)
		{
			case "view":
				$m_sanpham = $models->selectAllSanPham();
				break;    
			case "add":
			  	$models->addSanPham($_POST['masanpham'],$_POST['tensanpham'],$_POST['maloai'],$_POST['maco'],
				                    $_POST['mamau'],$_POST['chatlieu'],$_POST['ngaynhap'],$_POST['soluong']
									,$_POST['anh'],$_POST['dongiaban']);
				break;  				
			case "edit":
			  	$models->editSanPham($_POST['masanpham'],$_POST['tensanpham'],$_POST['maloai'],$_POST['maco'],
				                     $_POST['mamau'],$_POST['chatlieu'],$_POST['ngaynhap'],$_POST['soluong']
									,$_POST['anh'],$_POST['dongiaban']);
				break;        
			case "del":
				$models->delSanPham($_POST['masanpham']);
				break;    
		}
	}
?>

