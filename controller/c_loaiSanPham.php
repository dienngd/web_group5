<?php
	include("../models/m_loaiSanPham.php");
	$models = new m_loaiSanPham();
	$task = $_GET['task'];
	
	if($task)    
	{
		switch($task)
		{
			case "view":
				$m_loaiSanPham = $models->selectAllLoaiSanPham();
				break;    
			case "add":
			  	$models->addLoaiSanPham($_POST['maloai'],$_POST['tenloai']);
				break;  				
			case "edit":
			  	$models->editLoaiSanPham($_POST['maloai'],$_POST['tenloai']);
				break;        
			case "del":
				$models->delLoaiSanPham($_POST['maloai']);
				break;    
		}
	}
?>