<?php
include_once "Controller.php";
class sanPhamController extends Controller {
    public function index()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->index();

        include_once "views/index.php";
    }

    public function ao()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->ao();

        include_once "views/index.php";
    }
    
  
    public function aosomi()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->aosomi();

        include_once "views/index.php";
    }

    public function aothun()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->aothun();

        include_once "views/index.php";
    }

    public function aosanphammoi()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->aosanphammoi();

        include_once "views/index.php";
    }

    public function vay()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->vay();

        include_once "views/index.php";
    }

    public function vaydam()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->vaydam();

        include_once "views/index.php";
    }
    public function vayyem()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->vayyem();

        include_once "views/index.php";
    }

    public function vaysanphammoi()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->vaysanphammoi();

        include_once "views/index.php";
    }
    public function quanau()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->quanau();

        include_once "views/index.php";
    }
    public function quanbaggy()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->quanbaggy();

        include_once "views/index.php";
    }
    public function quan()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->quan();

        include_once "views/index.php";
    }
    public function quansooc()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->quansooc();

        include_once "views/index.php";
    }
    public function chanvay()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->chanvay();

        include_once "views/index.php";
    }
    public function vaynew()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->vaynew();

        include_once "views/index.php";
    }
    public function aonew()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->aonew();

        include_once "views/index.php";
    }
    public function quannew()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->quannew();

        include_once "views/index.php";
    }
    public function multi()
    {
        include_once "models/SanPham.php";
        $sp = new SanPham();
        $sanPham = $sp->multi();

        include_once "views/index.php";
    }
}