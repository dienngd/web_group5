<?php
class Load{
    function __construct($controller, $action)
    {
        // Kiểm tra file controller có tồn tại hay không
        if (!file_exists('controller/'.$controller.'.php')){
            die ('Controller không tồn tại');
        }

        require_once 'controller/'.$controller . '.php';

        // Kiểm tra class controller có tồn tại hay không
        if (!class_exists($controller)){
            die ('Controller không tồn tại2');
        }

        // Khởi tạo controller
        $controllerObject = new $controller();

        // Kiểm tra action có tồn tại hay không
        if ( !method_exists($controllerObject, $action)){
            die ('Action không tồn tại');
        }
        // Gọi tới action
        $controllerObject->{$action}();
    }
}